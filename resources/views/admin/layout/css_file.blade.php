    <!-- Custom fonts for this template-->
<link href="https://kit-pro.fontawesome.com/releases/v5.15.3/css/pro.min.css" rel="stylesheet" type="text/css"> 
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">

<!-- Custom css for this template-->

<link href="{{asset('/public/backend/css/bootstrap.min.css')}}" rel="stylesheet"> 
<link href="{{asset('/public/backend/css/bootstrap-icons.css')}}" rel="stylesheet">
<link href="{{asset('/public/backend/css/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{asset('/public/backend/css/datatables.min.css')}}" rel="stylesheet">
<link href="{{asset('/public/backend/css/sweetalert.min.css')}}" rel="stylesheet">
<link href="{{asset('/public/backend/css/daterangepicker.css')}}" rel="stylesheet">
<link href="{{asset('/public/backend/css/app.min.css')}}" rel="stylesheet">
<link href="{{asset('/public/backend/css/style.css')}}" rel="stylesheet">