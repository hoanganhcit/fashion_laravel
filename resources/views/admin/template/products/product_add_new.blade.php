@extends('admin.dashboard')
@section('title', 'Đăng sản phẩm mới')
@section('title-page', 'Đăng sản phẩm mới')
@section('content')
<!-- content -->
<div class="content ">

    <div class="mb-4">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{URL::to('dashboard')}}">
                        <i class="bi bi-globe2 small me-2"></i> Tổng quan
                    </a>
                </li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{URL::to('all-product-list')}}">Danh sách sản phẩm</a></li>
                <li class="breadcrumb-item active" aria-current="page">Đăng sản phẩm mới</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body mb-4">
                <?php
                $message = Session::get('message');
                if($message){
                    echo '<div class="alert alert-success"><span class="text-alert">'.$message.'</span></div>';
                    Session::put('message',null);
                }
                ?>
                <div class="position-center col-md-12">
                    <form role="form " class="row" action="{{URL::to('/save-product')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-5 ">
                            <h3 class="mb-4">Thông tin cơ bản</h3>
                            <div class="form-group">
                                <label for="product_image">Hình ảnh sản phẩm</label>
                                <div class="input-group form-control">
                                    <input id="upload" type="file" onchange="readURL(this);" required  name="product_image" class="form-control">
                                    <label id="upload-label" for="upload" class="font-weight-light text-muted"></label>
                                    <div class="input-group-append">
                                        <label for="upload" class="btn btn-light m-0 rounded-pill px-4"> 
                                            <i class="fa fa-cloud-upload mr-2 text-muted"></i>
                                            <small class="text-uppercase font-weight-bold text-muted">Chọn hình ảnh</small>
                                        </label>
                                    </div>
                                </div>
                                <!-- Uploaded image area-->
                                <div class="image-area mt-4" style="display: none;">
                                    <img id="imageResult" src="#" alt="" class="img-fluid ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="product_image">Gallery hình ảnh sản phẩm</label>
                                <input type="file" id="gallery-product" multiple required name="gallery_image[]" class="form-control">
                                <div class="preview-gallery "></div>
                            </div>
                            <div class="form-group">
                                <label for="">Mô tả sản phẩm</label>
                                <textarea style="resize: none" rows="6" required class="form-control" name="product_desc"  placeholder="Mô tả danh mục" id=""></textarea>
                            </div>
                        </div>
                        <div class="col-md-7 pl-5">
                            <h3 class="mb-4">Thông tin bán hàng</h3>
                            <div class="row"> 
                                <div class="form-group col-md-6">
                                    <label for="product_name">Tên sản phẩm</label>
                                    <input type="text"  class="form-control" onkeyup="ChangeToSlug();" required name="product_name"  id="name" placeholder="Tên sản phẩm" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="category_name">Đường dẫn</label>
                                    <input type="text"  class="form-control" name="product_slug"  id="convert_slug">
                                </div> 
                                <div class="form-group  col-md-6">
                                    <label for="">Giá bán</label>
                                    <input type="text"  class="form-control" name="product_price"  required id="" placeholder="Giá bán" >
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="">Giá khuyễn mãi <i class="fad fa-tags"></i></label>
                                    <input type="text" name="product_price_sale" class="form-control"  value="0" placeholder="Giá khuyễn mãi">
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="">Số lượng trong kho</label>
                                    <input type="number" name="product_qty" class="form-control"  required placeholder="Số lượng">
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="">Mã sản phẩm</label>
                                    <input type="text" name="product_sku" class="form-control" required placeholder="Mã sản phẩm">
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="">Tình trạng</label>
                                    <select name="product_status" class="form-control input-sm m-bot15"> 
                                        <option value="1">Còn hàng</option>
                                        <option value="0">Hết hàng</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="">Từ khóa sản phẩm</label>
                                    <input type="text" class="form-control tagsinput-example" name="product_keywords" placeholder="Từ khoá" value="">
                                </div> 
                                <div class="form-group col-md-6">
                                    <label for="exampleInputPassword1">Danh mục</label>
                                    <select name="category_product" class="form-control input-sm select2-example ">
                                        {!! $htmlOption !!}
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputPassword1">Thương Hiệu</label>
                                    <select name="brand_product" class="form-control input-sm m-bot15">
                                        @if(count($brand_product))
                                        @foreach($brand_product as $key => $val_brand)
                                        <option value="{{$val_brand->brand_id}}">{{$val_brand->brand_name}}</option>
                                        @endforeach
                                        @else
                                        <option value="">Chưa có thương hiệu</option>
                                        @endif
                                    </select>
                                </div> 
                                <div class="form-group col-md-6 col-xs-12">
                                    <label for="">Phân loại sản phẩm</label>
                                    <select class="form-control" name="product_type">
                                        <option value="1">Sản phẩm mới</option>
                                        <option value="2">Sản phẩm nổi bật</option>
                                        <option value="3">Sản phẩm độc quyền</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h3 class="mb-4">Thuộc tính sản phẩm</h3>
                            <div class="attributes_product_content">
                                {{-- <div class="mb-4">
                                    <a href="javascript:;" class="btn attributes_add--row"><i class="fal fa-plus-circle"></i> Thêm thuộc tính</a>
                                </div> --}}
                                <div class="attributes_rows">
                                    <div class="attributes_div mb-3"> 
                                        <div class="form-group mb-2 col-md-4 col-xs-12">
                                            <label class="text-3 mb-2">Size</label> 
                                        </div>
                                        <div class="attributes_div--child mb-3">
                                            <p>Chi tiết thuộc tính </p>
                                            <div class="attributes_item--child row mb-2">
                                                <div class="col-md-4 col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Nhập thuộc tính , ví dụ: S , M , L ..." name="attributes_value[]">
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Số lượng trong kho" name="attributes_qty[]">
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Giá bán" name="attributes_price[]">
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-info btn-sm add_attr_item">Thêm chi tiết thuộc tính</button>
                                    </div>
                                    <div class="attributes_div mb-3"> 
                                        <div class="form-group mb-2 col-md-4 col-xs-12">
                                            <label>Màu sắc sản phẩm</label>
                                            <select class="form-control color_type" name="attributes_color_type">
                                                <option value="1">Màu sắc tùy chỉnh</option>
                                                <option value="2">Hình Ảnh Minh Hoạ</option>
                                            </select>
                                        </div>
                                        <div class="color-div mb-3">
                                            <div class="attributes_div--child">
                                                <p>Chi tiết thuộc tính </p>
                                                <div class="attributes_item--child row mb-2">
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="input-group">
                                                            <input type="color" class="form-control colorpicker" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$">
                                                            <input type="text"  pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$"  name="attributes_color_image[]" value="" class="form-control hexcolor" placeholder="#000000" style="text-transform: uppercase;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-xs-12">
                                                        <input type="text" class="form-control" placeholder="Nhập màu" name="attributes_color_value[]">
                                                    </div>
                                                    <div class="col-md-3 col-xs-12">
                                                        <input type="text" class="form-control" placeholder="Số lượng" name="attributes_color_qty[]">
                                                    </div>
                                                    <div class="col-md-3 col-xs-12">
                                                        <input type="text" class="form-control" placeholder="Giá bán" name="attributes_color_price[]">
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-info btn-sm add_color_item">Thêm chi tiết thuộc tính</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h3 class="mt-4 mb-4">Chi tiết sản phẩm</h3>
                            <div class="form-group">
                                <textarea style="resize: none" rows="8" class="form-control ckeditor"  name="product_content"  id="editor_content" placeholder="Nội dung sản phẩm"></textarea>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <button type="submit" name="add_product" class="btn btn-primary">Thêm sản phẩm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- ./ content -->
@endsection
