@extends('admin.dashboard')
@section('title', 'Tổng Quan')
@section('title-page', 'Tổng Quan')
@section('content')
<!-- content -->
<div class="content">
    <div class="row g-4 mb-4">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="card border-0">
                <a href="{{URL::to('orders')}}">
                    <div class="card-body text-center">
                        <div class="display-5">
                            <i class="bi bi-basket text-info"></i>
                            @if($order_new->count() > 0)
                            <span class="bagde count_number">{{$order_new->count()}}</span>
                            @endif
                        </div>
                        <h5 class="my-3 text-black">Đơn Hàng Mới</h5>
                        <div class="text-muted">Hiện có {{$order_new->count()}} đơn hàng mới</div> 
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="card border-0">
                <div class="card-body text-center">
                    <div class="display-5">
                        <i class="fal fa-badge-dollar text-success"></i>
                    </div>
                    <h5 class="my-3">{{number_format($orders)}} đ</h5>
                    <div class="text-muted">Tổng doanh thu</div> 
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="card border-0">
                <div class="card-body text-center">
                    <div class="display-5">
                        <i class="fal fa-box text-warning"></i>
                    </div>
                    <h5 class="my-3">Sản Phẩm</h5>
                    <div class="text-muted">Có {{$products_total}} sản phẩm</div> 
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="card border-0">
                <div class="card-body text-center">
                    <div class="display-5">
                        <i class="fal fa-users text-danger"></i>
                    </div>
                    <h5 class="my-3">Thành viên</h5>
                    <div class="text-muted">Có {{$customer_total}} thành viên</div> 
                </div>
            </div>
        </div>
    </div>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        <div class="col-lg-8 col-md-12">
            <div class="card widget h-100">
                <div class="card-header d-flex">
                    <h6 class="card-title">
                        Doanh số bán hàng
                        <a href="#" class="bi bi-question-circle ms-1 small" data-bs-toggle="tooltip"
                        title="Đơn đặt hàng hàng ngày và doanh số bán hàng"></a>
                    </h6>
                </div>
                <div class="card-body">
                    <div class="d-md-flex align-items-center mb-3">
                        <div class="d-flex align-items-center">
                            <div class="display-7 me-3">
                                <i class="bi bi-bag-check me-2 text-success"></i> {{number_format($total_order)}} ₫
                            </div>
                        </div>
                        <div class="d-flex gap-4 align-items-center ms-auto mt-3 mt-lg-0">
                            <select class="form-select" id="preview-chart">
                                <option {{ Request::get('preview') == 'this_week' ? ' selected ' : '' }} value="{{Request::url()}}?preview=this_week" value="this_week" selected >Doanh thu tuần này</option>
                                <option {{ Request::get('preview') == 'last_week' ? ' selected ' : '' }} value="{{Request::url()}}?preview=last_week" value="last_week">Doanh thu tuần trước</option> 
                                <option {{ Request::get('preview') == 'this_month' ? ' selected ' : '' }} value="{{Request::url()}}?preview=this_month" value="this_month" >Doanh thu tháng này</option>
                                <option {{ Request::get('preview') == 'last_month' ? ' selected ' : '' }} value="{{Request::url()}}?preview=last_month" value="last_month">Doanh thu tháng trước</option>
                            </select>
                        </div>
                    </div>
                    <div id="sales-chart" data-list-day="{{json_encode($listDay)}}" data-revenue="{{json_encode($arrayRevenueOrderMonth)}}" data-count="{{json_encode($arrayCountOrderMonth)}}" ></div>
                    <div class="d-flex justify-content-center gap-4 align-items-center ms-auto mt-3 mt-lg-0">
                        <div>
                            <i class="bi bi-circle-fill mr-2 text-success me-1 small"></i>
                            <span>Doanh số</span>
                        </div>
                        <div>
                            <i class="bi bi-circle-fill mr-2 text-primary me-1 small"></i>
                            <span>Số đơn hàng</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="card widget h-100">
                <div class="card-header d-flex">
                    <h6 class="card-title">
                        Top sản phẩm bán chạy
                        <a href="#" class="bi bi-question-circle ms-1 small" data-bs-toggle="tooltip"
                        title="Thống kê sản phẩm bán chạy trong tháng"></a>
                    </h6>
                </div>
                <div class="card-body">
                    <div class="list-group list-group-flush">
                        @foreach($best_sellers as $product)
                        <div class="list-group-item d-flex justify-content-between align-items-center px-0">
                            <div class="d-flex flex-grow-1 align-items-center">
                                <img width="45" class="rounded" src="{{asset('public/'.$product->product_image_path)}}" alt="{{$product->product_name}}">
                                <div class="pl-2 d-flex flex-column">
                                    <span style="font-weight: 500">{{$product->product_name}}</span>
                                    @if($product->product_price_sale > 0)
                                    <span>{{number_format($product->product_price_sale)}} đ</span>
                                    @else
                                    <span>{{number_format($product->product_price)}} đ</span>
                                    @endif
                                    <span>Đã bán : {{$product->product_sold}}</span>
                                </div> 
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./ content -->
@endsection
