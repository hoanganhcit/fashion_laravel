<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta property="og:url" content=""/>
    <meta property="og:title" content=""/>
    <meta property="og:description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="">
    <meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/public/backend/images/favicon.png')}}">

    <title>@yield('title')</title>

    <!-- Styles -->
    @include('admin.layout.css_file')
</head>
<body>
    <!-- preloader -->
    <div class="preloader">
        <img src="{{asset('public/backend/images/logo.png')}}" alt="logo">
        <div class="preloader-icon"></div>
    </div>
    <!-- ./ preloader -->
    <!-- menu -->
    @include('admin.layout.sidebar')
    <!-- ./  menu -->
    <!-- layout-wrapper -->
    <div class="layout-wrapper">
        <!-- header -->
        @include('admin.layout.header')
        <!-- ./ header -->
        <!-- Content -->
        @yield('content')
        <!-- Content end -->
        <!-- content-footer -->
        @include('admin.layout.footer')
        <!-- ./ content-footer -->
    </div>
</div>
<!-- Scripts -->
@include('admin.layout.js_file')

<script src="{{asset('public/backend/js/dashboard.js')}}"></script> 

<script>
    $(document).on("change","#preview-chart", function () {
        var url = $(this).val();  
        if (url) { 
            window.location = url;
        }
        return false;
    });
    $(document).on("input",".colorpicker", function () {
        $(this).parent().find('.hexcolor').val(this.value);
    });
    $(document).on("input",".hexcolor", function () {
        $(this).parent().find('.colorpicker').val(this.value);
    });
    $(document).ready(function(){
        
        $('#datatable-search').DataTable({
            responsive: true
        });
        $('.date_coupon').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });

        $('.attributes_add--row').click(function(){
            $('.attributes_rows').append('<div class="attributes_div mb-3"><button class="btn btn-danger remove_attributes_rows btn-sm">Xoá</button><div class="form-group mb-2 col-md-6"><label>Tên thuộc tính</label><input type="text" class="form-control" placeholder="Nhập tên thuộc tính, ví dụ: màu sắc, size v.v" name="attributes_name[]"></div><div class="attributes_div--child mb-3"><p>Chi tiết thuộc tính </p><div class="attributes_item--child row mb-2"><div class="col-md-4 col-xs-12"><input type="text" class="form-control" placeholder="Nhập thuộc tính , ví dụ: S , M , L ..." name="attributes_value[]"></div><div class="col-md-4 col-xs-12"><input type="text" class="form-control" placeholder="Số lượng trong kho" name="attributes_qty[]"></div><div class="col-md-4 col-xs-12"><input type="text" class="form-control" placeholder="Giá bán" name="attributes_price[]"></div></div></div><button type="button" class="btn btn-info btn-sm add_attr_item">Thêm chi tiết thuộc tính</button></div>');
        }); 
        $(document).on("click",".add_attr_item", function () {
            $(this).parent().find('.attributes_div--child').append('<div class="attributes_item--child row mb-2"><div class="col-md-4 col-xs-12"><input type="text" class="form-control" placeholder="Nhập thuộc tính , ví dụ: S , M , L ..." name="attributes_value[]"></div><div class="col-md-4 col-xs-12"><input type="text" class="form-control" placeholder="Số lượng trong kho" name="attributes_qty[]"></div><div class="col-md-4 col-xs-12"><input type="text" class="form-control" placeholder="Giá bán" name="attributes_price[]"></div><button type="button" class="btn btn-danger btn-sm remove_attr"><i class="fad fa-trash"></i></button></div>');
        });


        $(document).on("click",".add_color_item", function () {
            $(this).parent().find('.attributes_div--child').append('<div class="attributes_item--child row mb-2"><div class="col-md-3 col-xs-12"><div class="input-group"><input type="color" class="form-control colorpicker" id=""  pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$"><input type="text"  pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" name="attributes_color_image[]" value="" class="form-control hexcolor" placeholder="#000000" style="text-transform: uppercase;"></div></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Nhập màu" name="attributes_color_value[]"></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Số lượng" name="attributes_color_qty[]"></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Giá bán" name="attributes_color_price[]"></div><button type="button" class="btn btn-danger btn-sm remove_attr"><i class="fad fa-trash"></i></button></div>');

            $('.colorpicker').on('input', function() {
                $(this).parent().find('.hexcolor').val(this.value);
            });
            $('.hexcolor').on('input', function() {
                $(this).parent().find('.colorpicker').val(this.value);
            });
        });
        $(document).on("click",".add_image_item", function () {
            $(this).parent().find('.attributes_div--child').append('<div class="attributes_item--child row mb-2"><div class="col-md-4 col-xs-12"><input type="file" class="form-control" name="attributes_color_image[]"></div><div class="col-md-2 col-xs-12"><input type="text" class="form-control" placeholder="Màu" name="attributes_color_value[]"></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Số lượng" name="attributes_color_qty[]"></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Giá bán" name="attributes_color_price[]"></div><button type="button" class="btn btn-danger btn-sm remove_attr"><i class="fad fa-trash"></i></button></div>');

            $('.colorpicker').on('input', function() {
                $(this).parent().find('.hexcolor').val(this.value);
            });
            $('.hexcolor').on('input', function() {
                $(this).parent().find('.colorpicker').val(this.value);
            });
        });
        $(document).on("change",".color_type", function () {
            if( $(this).val() == '1') {
                $('.image-div').remove();
                $(this).parent().parent().append('<div class="color-div mb-3"><div class="attributes_div--child"><p>Chi tiết thuộc tính </p><div class="attributes_item--child color_childred row mb-2"><div class="col-md-3 col-xs-12"><div class="input-group"><input type="color" class="form-control colorpicker" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$"><input type="text"  pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" name="attributes_color_image[]" value="" class="form-control hexcolor" placeholder="#000000" style="text-transform: uppercase;"></div></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Nhập màu" name="attributes_color_value[]"></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Số lượng" name="attributes_color_qty[]"></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Giá bán" name="attributes_color_price[]"></div></div></div><button type="button" class="btn btn-info btn-sm add_color_item">Thêm chi tiết thuộc tính</button></div>');
            }else if( $(this).val() == '2' ) {
                $('.color-div').remove();
                $(this).parent().parent().append('<div class="image-div mb-3"><div class="attributes_div--child"><p>Chi tiết thuộc tính </p><div class="attributes_item--child color_childred row mb-2"><div class="col-md-4 col-xs-12"><input type="file" class="form-control" name="attributes_color_image[]"></div><div class="col-md-2 col-xs-12"><input type="text" class="form-control" placeholder="Màu" name="attributes_color_value[]"></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Số lượng" name="attributes_color_qty[]"></div><div class="col-md-3 col-xs-12"><input type="text" class="form-control" placeholder="Giá bán" name="attributes_color_price[]"></div></div></div><button type="button" class="btn btn-info btn-sm add_image_item">Thêm chi tiết thuộc tính</button></div>');;

            }
        });
        $(document).on("click",".remove_attr, .remove_attributes_rows", function () {
            $(this).parent().remove();
        });
        

        ClassicEditor.create( document.querySelector( '#editor_content' ), {
            image: {
                toolbar: [ 'toggleImageCaption', 'imageTextAlternative' ]
            }
        });
        
    });
    $('.select_address').on('change', function() {
        var action = $(this).attr('id');
        var ma_id = $(this).val();
        var result = '';

        if(action == 'city'){
            result = 'province';
        }else {
            result = 'ward';
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '{{url('/delivery/select-delivery')}}',
            crossDomain: true,
            data:{action:action, ma_id:ma_id},
            success: function (data) { 
                $('#'+result).html(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });

    $(function () { 
        attributes_value();
        attributes_color();

    });
    function attributes_value() {
        $.ajax({
            url:'{{url('/attributes-value')}}',
            method:"GET",
            success:function(data){
                $('.attr_value_render').html(data);
            }
        }); 
    } 
    function attributes_color() {
        $.ajax({
            url:'{{url('/attributes-color')}}',
            method:"GET",
            success:function(data){
                $('.attributes_color_render').html(data); 
            }
        }); 
    } 
    $(document).on("click",".delete-attributes", function () { 
        event.preventDefault();
        var attr_id = $(this).data('id'); 
        swal({
            title: "Thông báo !",
            text: "Bạn có chắc muốn xoá thuộc tính này?",
            type: "error",
            icon: "danger",
            showCancelButton: true,
            dangerMode: true,
            cancelButtonClass: '#DD6B55',
            confirmButtonColor: '#dc3545',
            confirmButtonText: 'Xoá',
            cancelButtonText: 'Huỷ bỏ',
        },function (result) {
            if (result) {
                var seff = $('.attr_value_render');
                seff.find('.loading').remove();
                seff.append('<div class="loading"><img src="{!! asset('public/frontend/images/loader.svg') !!}" class="fa-spin"></div>');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '{{url('/attributes/delete')}}',
                    crossDomain: true,
                    data:{attr_id:attr_id},
                    success: function (data) {
                        $('.loading').remove();
                        $('.attr_value_render').html(data);
                        attributes_value();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }); 
    }); 
    $(document).on("click",".update-attributes", function () { 
        event.preventDefault();
        var attr_id = $(this).data('id'); 
        var attr_name = $(this).parent().parent().find('.attr_name').val();
        var attr_qty = $(this).parent().parent().find('.attr_qty').val();
        var attr_price = $(this).parent().parent().find('.attr_price').val();
        // alert(attr_name);
        var seff = $('.attr_value_render');
        seff.find('.loading').remove();
        seff.append('<div class="loading"><img src="{!! asset('public/frontend/images/loader.svg') !!}" class="fa-spin"></div>');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '{{url('/attributes/update')}}',
            crossDomain: true,
            data:{attr_id:attr_id,attr_name:attr_name,attr_qty:attr_qty,attr_price:attr_price},
            success: function (data) {
                $('.loading').remove();
                $('.attr_value_render').html(data);
                attributes_value();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        }); 
    }); 

    $(document).on("click",".delete-color", function () { 
        event.preventDefault();
        var attr_id = $(this).data('id'); 
        swal({
            title: "Thông báo !",
            text: "Bạn có chắc muốn xoá thuộc tính này?",
            type: "error",
            icon: "danger",
            showCancelButton: true,
            dangerMode: true,
            cancelButtonClass: '#DD6B55',
            confirmButtonColor: '#dc3545',
            confirmButtonText: 'Xoá',
            cancelButtonText: 'Huỷ bỏ',
        },function (result) {
            if (result) {
                var seff = $('.attributes_color_render');
                seff.find('.loading').remove();
                seff.append('<div class="loading"><img src="{!! asset('public/frontend/images/loader.svg') !!}" class="fa-spin"></div>');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '{{url('/attributes-color/delete')}}',
                    crossDomain: true,
                    data:{attr_id:attr_id},
                    success: function (data) {
                        $('.loading').remove();
                        $('.attributes_color_render').html(data);
                        attributes_color();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }); 
    }); 

    $(document).on("click",".update-color", function () { 
        event.preventDefault();
        var attr_id = $(this).data('id'); 
        var color_value = $(this).parent().parent().find('.color_value').val();
        var color_image = $(this).parent().parent().find('.color_image').val();
        var color_qty = $(this).parent().parent().find('.color_qty').val();
        var color_price = $(this).parent().parent().find('.color_price').val();
        // alert(color_price);
        var seff = $('.attributes_color_render');
        seff.find('.loading').remove();
        seff.append('<div class="loading"><img src="{!! asset('public/frontend/images/loader.svg') !!}" class="fa-spin"></div>');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '{{url('/attributes-color/update')}}',
            crossDomain: true,
            data:{attr_id:attr_id,color_value:color_value,color_image:color_image,color_qty:color_qty,color_price:color_price},
            success: function (data) {
                $('.loading').remove();
                $('.attributes_color_render').html(data);
                attributes_color();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        }); 
    }); 
</script>
</body>
</html>
