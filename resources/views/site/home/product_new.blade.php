    <div class="row wpb_row mb150">
        <div class="col-sm-12">
            <div class="content-editor h3-title ">
                <div class="content-info ">
                    <h3 class="title24 font-bold text-uppercase mb-4">Sản phẩm mới</h3>
                </div>
            </div>
            <div class="tabs-block block-element h3-tabs tab-style2 tab-ajax-off">
                <div class="vc_tta-panel-body">
                    <div class="block-element  product-slider-view  slider filter- js-content-wrap">
                        <div class="list-products-wrap">
                            <div class="wrap-item smart-slider js-content-main clearfix group-navi " data-item="4" data-speed="" data-itemres="" data-prev="" data-next=""  data-pagination="" data-navigation="group-navi">
                                @foreach($product_new as $product_new)
                                <div class="item">  
                                    <div class="product type-product status-publish has-post-thumbnail">
                                        <div class="item-product item-product-grid item-product-style2">
                                            <div class="product-thumb">
                                                <a href="{{URL::to('/san-pham/'.$product_new->product_slug)}}" class="product-thumb-link product-thumb-link zoomout-thumb" > 
                                                    <img  src="{{asset('/public/'.$product_new->product_image_path)}}" alt="{{$product_new->product_image}}" /> 
                                                    <img  src="{{asset('/public/'.$product_new->product_image_path)}}" alt="{{$product_new->product_image}}" /> 
                                                </a>       
                                                <div class="product-label">
                                                    @if($product_new->product_price_sale > 0)
                                                    <span class="sale">Sale</span>
                                                    @endif
                                                </div> 

                                                <div class="product-extra-link">
                                                    <button class="btn btn-primary product-quick-view quickview-link" data-bs-target="#quickview" data-bs-toggle="modal" data-id="{{$product_new->product_id}}"><i class="fal fa-shopping-bag"></i> <span>Mua nhanh</span></button>
                                                    <a href="{{URL::to('/san-pham/'.$product_new->product_slug)}}" class="btn btn-primary" data-id="{{$product_new->product_id}}"><span>Xem chi tiết</span> <i class="fal fa-long-arrow-right"></i></a>
                                                </div> 
                                            </div>
                                            <div class="product-info">
                                                <h3 class="title14 product-title">
                                                    <a title="{{$product_new->product_name}}" href="{{URL::to('/san-pham/'.$product_new->product_slug)}}">{{$product_new->product_name}}</a>
                                                </h3>
                                                <div class="product-price price variable">
                                                    @if($product_new->product_price_sale > 0)
                                                    <span class="price-sale">{{number_format($product_new->product_price)}} đ</span>
                                                    <span class="Price-amount">{{number_format($product_new->product_price_sale)}} đ</span>
                                                    <div class="product-label"><span class="sale-per">Giảm {{ number_format((float)100 - ($product_new->product_price_sale / $product_new->product_price * 100), 0, '.', '') }}%</span></div>
                                                    @else
                                                    <span class="Price-amount">{{number_format($product_new->product_price)}} đ</span>
                                                    @endif
                                                </div>   
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>