@if($categoryParent->childs->count())
    <div class="row wpb_row mb-4">
        <div class="col-sm-12">
            <div class="content-editor h3-title ">
                <div class="content-info mb-4">
                    <a href="{{URL::to('danh-muc/' .$categoryParent->category_slug)}}"><h3 class="title24 font-bold text-uppercase mr-4">{{$categoryParent->category_name}}</h3></a>
                    <ul class="title-tab font-bold text-right  list-inline-block nav nav-tabs" role="tablist">
                        @foreach($categoryParent->childs as $categoryChild)
                        <li>
                            <a href="#" class="tab-item click_product_by_category" data-id="{{$categoryChild->category_id}}" data-parentid="{{$categoryParent->category_id}}">{{$categoryChild->category_name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="tabs-block block-element h3-tabs tab-style2 ">
                <div class="vc_tta-panel-body">
                    <div class="block-element ">
                        <div class="list-product-category">
                            <div class="mb-4" id="waiting-{{$categoryParent->category_id}}"></div>
                            @if($categoryParent->products->count())
                            <div class="product-category-grid" id="category-{{$categoryParent->category_id}}">
                                @foreach($categoryParent->products->take(8) as $product)
                                <div class="item">  
                                    <div class="product type-product status-publish has-post-thumbnail">
                                        <div class="item-product item-product-grid item-product-style2">
                                            <div class="product-thumb">
                                                <a href="{{URL::to('/san-pham/'.$product->product_slug)}}" class="product-thumb-link product-thumb-link zoomout-thumb" > 
                                                    <img  src="{{asset('/public/'.$product->product_image_path)}}" alt="{{$product->product_image}}" /> 
                                                    <img  src="{{asset('/public/'.$product->product_image_path)}}" alt="{{$product->product_image}}" /> 
                                                </a>       
                                                <div class="product-label">
                                                    @if($product->product_price_sale > 0)
                                                    <span class="sale">Sale</span>
                                                    @endif
                                                </div> 

                                                <div class="product-extra-link">
                                                    <button class="btn btn-primary product-quick-view quickview-link" data-bs-target="#quickview" data-bs-toggle="modal" data-id="{{$product->product_id}}"><i class="fal fa-shopping-bag"></i> <span>Mua nhanh</span></button>
                                                    <a href="{{URL::to('/san-pham/'.$product->product_slug)}}" class="btn btn-primary" data-id="{{$product->product_id}}"><span>Xem chi tiết</span> <i class="fal fa-long-arrow-right"></i></a>
                                                </div> 
                                            </div>
                                            <div class="product-info">
                                                <h3 class="title14 product-title">
                                                    <a title="{{$product->product_name}}" href="{{URL::to('/san-pham/'.$product->product_slug)}}">{{$product->product_name}}</a>
                                                </h3>
                                                <div class="product-price price variable">
                                                    @if($product->product_price_sale > 0)
                                                    <span class="price-sale">{{number_format($product->product_price)}} đ</span>
                                                    <span class="Price-amount">{{number_format($product->product_price_sale)}} đ</span>
                                                    <div class="product-label"><span class="sale-per">Giảm {{ number_format((float)100 - ($product->product_price_sale / $product->product_price * 100), 0, '.', '') }}%</span></div>
                                                    @else
                                                    <span class="Price-amount">{{number_format($product->product_price)}} đ</span>
                                                    @endif
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @else
                            <div class="empty-product center">
                                <p>Chưa có sản phẩm nào</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif