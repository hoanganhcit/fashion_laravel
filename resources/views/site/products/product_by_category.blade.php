@extends('index')
@section('layout')

<div class="container">
	<div class="wrap-bread-crumb">
		<div class="bread-crumb"> 
			<div class="bread-crumb-wrap">
				<span><a href="{{URL::to('/')}}"><i class="fad fa-home mr-3"></i> Home</a></span>
				<span>Shops</span>       		
			</div>
		</div>
	</div>
	<div class="row mt-4">
		@include('site.layout.sidebar')
		<div class="main-wrap-shop content-wrap content-sidebar-right col-md-9 col-sm-8 col-xs-12">
			<div class="title-page clearfix">
				<div class="title-box d-flex justify-between">
					<h3 class="title-cate">{{$category_name->category_name}} <span>Có {{$products->count()}} sản phẩm</span></h3>
					<ul class="sort-pagi-bar list-inline-block pull-right">
						<li>
							<div class="sort-by grid-filter">
								<span class="black">Sắp xếp:</span>
								<select name="sortby" class="form-control " id="sortBy">
									<option {{ Request::get('sort_by') == 'default' ? ' selected ' : '' }} value="{{Request::url()}}?sort_by=default" selected>Mặc định</option>
									<option {{ Request::get('sort_by') == 'desc' ? ' selected ' : '' }} value="{{Request::url()}}?sort_by=desc">Mới nhất</option> 
									<option {{ Request::get('sort_by') == 'price_max' ? ' selected ' : '' }} value="{{Request::url()}}?sort_by=price_max">Giá giảm dần</option>
									<option {{ Request::get('sort_by') == 'price_min' ? ' selected ' : '' }} value="{{Request::url()}}?sort_by=price_min">Giá tăng dần</option>
								</select>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="product-grid-view products-wrap js-content-wrap content-sidebar-right">
				@if($products->count())
				<div class="products row list-product-wrap js-content-main">
					@foreach($products as $product)
					<div class="list-col-item list-3-item">
						<div class="item-product item-product-grid item-product-default">
							<div class="product type-product status-publish has-post-thumbnail">
								<div class="item-product item-product-grid item-product-style2">
									<div class="product-thumb">
										<a href="{{URL::to('/san-pham/'.$product->product_slug)}}" class="product-thumb-link product-thumb-link zoomout-thumb" >
											<img  src="{{asset('/public/'.$product->product_image_path)}}" alt="{{$product->product_name}}" />
											<img  src="{{asset('/public/'.$product->product_image_path)}}" alt="{{$product->product_name}}" />
										</a>  
										<div class="product-label">
											@if($product->product_price_sale > 0)
											<span class="sale">Sale</span>
											@endif
											<span class="featured">Mới</span>
										</div>  
										<div class="product-extra-link">
											<button class="btn btn-primary product-quick-view quickview-link" data-bs-target="#quickview" data-bs-toggle="modal" data-id="{{$product->product_id}}"><i class="fal fa-shopping-bag"></i> <span>Mua nhanh</span></button>
											<a href="{{URL::to('/san-pham/'.$product->product_slug)}}" class="btn btn-primary" data-id="{{$product->product_id}}"><span>Xem chi tiết</span> <i class="fal fa-long-arrow-right"></i></a>
										</div>     
									</div>
									<div class="product-info">
										<h3 class="title14 product-title">
											<a title="Laborum Chair" href="{{URL::to('/san-pham/'.$product->product_slug)}}">{{$product->product_name}}</a>
										</h3>
										<div class="product-price">
											@if($product->product_price_sale > 0)
											<span class="price-sale">{{number_format($product->product_price)}} đ</span>
											<span class="Price-amount">{{number_format($product->product_price_sale)}} đ</span>
											<div class="product-label"><span class="sale-per">Giảm {{ number_format((float)100 - ($product->product_price_sale / $product->product_price * 100), 0, '.', '') }}%</span></div>
											@else
											<span class="Price-amount">{{number_format($product->product_price)}} đ</span>
											@endif
										</div>   
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
				@else
				<div class="empty-product">
					<p>Không có sản phẩm nào phù hợp</p>
				</div>
				@endif
			</div>
			<div class="navigation-end">
                {{$products->links()}}
            </div>
		</div>
	</div> <!-- close row --> 
</div>
@endsection()