@extends('index')
@section('layout')
<style> 
	img.color-img {
		width: 100%;
		height: 100%;
		object-fit: cover;
		border-radius: 100%;
	}
</style>
<div class="container ">
	@foreach($data_product as $value)
	<div class="wrap-bread-crumb">
		<div class="bread-crumb"> 
			<div class="bread-crumb-wrap">
				<span><a href="{{URL::to('/')}}"><i class="fad fa-home mr-3"></i> Trang chủ</a></span>
				<span><a href="{{URL::to('danh-muc/'. $value->category_slug)}}">Sản phẩm</a></span>
				<span>{{$value->product_name}}</span>       		
			</div>
		</div>
	</div>
	@endforeach
	<div id="message"></div> 
	<div class="product-type-simple">
		<div class="product-detail main-detail-full">
			<div class="row">
				@foreach($data_product as $product)
				<div class="col-md-7 col-sm-12 col-xs-12">
					<div class="detail-gallery has-gallery">
						<div class="wrap-detail-gallery images zoom-style3">
							<div class="mid woocommerce-product-gallery__image image-lightbox true" data-gallery="">
								<a href="{{asset('/public/'.$product->product_image_path)}}" class="img_url">
									<img width="600" height="686" src="{{asset('/public/'.$product->product_image_path)}}" class="wp-post-image wp-post-image" alt="{{$product->product_name}}" srcset="{{asset('/public/'.$product->product_image_path)}}" />
								</a>
							</div>
							<div class="gallery-control true">
								<a href="#" class="prev"><i class="fal fa-arrow-down"></i></a>
								<div class="carousel" data-visible="4" data-vertical="true">
									<ul class="list-none">
										<li data-number="">
											<a class="active" href="{{asset('/public/'.$product->product_image_path)}}">
												<img width="100" height="114" src="{{asset('/public/'.$product->product_image_path)}}" class="attachment-100x114 size-100x114" alt="{{$product->product_name}}" data-src="{{asset('/public/'.$product->product_image_path)}}" data-srcset="{{asset('/public/'.$product->product_image_path)}}" />
											</a>
										</li>
										@foreach($galery_product_thumb as $thumb)
										<li data-number="">
											<a  href="{{asset('/public/'.$thumb->gallery_image_path)}}">
												<img width="100" height="114" src="{{asset('/public/'.$thumb->gallery_image_path)}}" class="attachment-100x114 size-100x114" alt="{{$product->product_name}}" data-src="{{asset('/public/'.$thumb->gallery_image_path)}}" data-srcset="{{asset('/public/'.$thumb->gallery_image_path)}}" />
											</a>
										</li>
										@endforeach                  
									</ul>
								</div>
								<a href="#" class="next"><i class="fal fa-arrow-up"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5 col-sm-12 col-xs-12">
					<div class="summary entry-summary detail-info">
						<h2 class="product-title title30 font-bold">{{$product->product_name}}</h2>
						<div class="product-price">
							@if($product->product_price_sale > 0)
							<span class="price-sale">{{number_format($product->product_price)}} đ</span>
							<span class="Price-amount">{{number_format($product->product_price_sale)}} đ</span>
							@else
							<span class="Price-amount">{{number_format($product->product_price)}} đ</span>
							@endif
						</div>
						@if(count($product_brand) > 0)
						<div class="list-brand">
							<ul class="list-inline-block">
								<li>
									@foreach($product_brand as $brand)
									<a href="{{URL::to('thuong-hieu/'.$brand->brand_slug)}}" class="float">
										<img width="100" height="57" src="{{asset('/public/uploads/brands/'.$brand->brand_logo)}}" class="attachment-100x57 size-100x57" alt="{{$brand->brand_name}}">
									</a>
									@endforeach
								</li>
							</ul>
						</div>
						@endif
						<div class="woocommerce-product-details__short-description">
							<div class="req clearfix" >
								<p>Tình Trạng: 
									@if($product->product_status == 1)
									<span class="text-success">Còn hàng</span>
									@else
									<span class="text-danger">Hết hàng</span>
									@endif
								</p>
							</div>
							@if($variation_color->count() > 0)
							<div class="size mb-3" >
								<div class="d-flex justify-content-between align-items-baseline mb-2 mt-3 mt-md-0">
									<label>Màu sắc</label>
								</div>
								<div class="list-color">
									@foreach($variation_color as $color)
									@if($color->attributes_color_type == 1)
										<a href="javascript:void(0)" data-bs-toggle="tooltip" title="{{$color->attributes_color_value}}" class="text-center d-inline-block color-item" data-value="{{$color->attributes_color_value}}">
											<span class="color-variation" style="background: {{$color->attributes_color_image}}"></span>
										</a>
									@else 
										<a href="javascript:void(0)" data-bs-toggle="tooltip" title="{{$color->attributes_color_value}}" class="text-center d-inline-block color-item" data-value="{{$color->attributes_color_value}}">
											<img src="{{asset('public/'.$color->attributes_color_image_path)}}" class="color-img" width="30" alt="{{$color->attributes_color_image}}">
										</a>
									@endif
									@endforeach
								</div>
							</div>
							@endif
							@if($variation_size->count() > 0)
							<div class="size mb-3" >
								<div class="d-flex justify-content-between align-items-baseline mb-2 mt-3 mt-md-0">
									<label>Size</label>
								</div>
								<div class="list-size">
									@foreach($variation_size as $size)
									<a href="javascript:void(0)" class="text-center d-inline-block size-item" data-bs-toggle="tooltip" title="Còn lại {{$size->attributes_qty}} sản phẩm" data-value="{{$size->attributes_value}}">{{$size->attributes_value}}</a>
									@endforeach
								</div>
							</div>
							@endif
							<div class="qty_row">
								<div class="detail-qty product-qty info-qty border radius6">
									<a href="javascript:;" class="qty-down"><i class="fal fa-minus-square" aria-hidden="true"></i></a>
									<input type="text" id="quantity" name="quantity" value="1" title="Qty" class="input-text text qty qty-val" />
									<a href="javascript:;" class="qty-up"><i class="fal fa-plus-square" aria-hidden="true"></i></a>
									<input type="hidden" name="quantity" id="quantity_val" value="1">
									<input type="hidden" id="product_name" value="{{$product->product_name}}">
								</div>
								<button type="submit" name="add-to-cart" value="304" class="single_add_to_cart_button button alt" data-id="{{$product->product_id}}">Thêm vào giỏ hàng</button>
							</div>
							<div class="clear"></div>
							<div class="product_meta item-product-meta-info">
								<div class="product-desc">
									<p>{{$product->product_desc}}</p>
								</div>
								<div class="sku_wrapper  mb-2">
									<label>Mã sản phẩm:</label> 
									<span class="sku">{{$product->product_sku}}</span>
								</div>
								<div class="ku_wrapper  mb-2">
									<label>Danh mục:</label>
									<span class="meta-item-list">
										<a href="{{URL::to('danh-muc/'.$product->category_slug)}}" rel="tag">{{$product->category_name}}</a>
									</span>
								</div>
								<div class="ku_wrapper  mb-2">
									<label>Tags:</label>
									@php 
									$tags = $product->product_keywords;
									$tags = explode(",",$tags);

									@endphp
									@foreach($tags as $tag)
									<span class="meta-tag">{{$tag}}</span>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
				{{-- <div class="single-products">
					{!!$product->product_content!!}
				</div> --}}
				@endforeach
			</div>
		</div>
		<div class="product-intro-wrapper productDetails d-flex flex-wrap">
			<div class="col-12 col-md-6">
				<div class="content-border">
					<h4 style="margin:0px;padding:12px 0px 8px;line-height:1.1;font-size:15px;text-transform:uppercase;"><strong>CHÍNH SÁCH ĐỔI TRẢ HÀNG</strong></h4>
					<div class="content" style="margin:0px;">
						<p>- Đổi hàng&nbsp;trong vòng 3 ngày kể từ khi nhận hàng</p>
						<p>- Miễn phí đổi trả nếu lỗi sai sót</p>
					</div>                
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="content-border">
					<h4 style="margin:0px;padding:12px 0px 8px;line-height:1.1;font-size:15px;text-transform:uppercase;"><strong>ƯU ĐÃI MEMBER VIP</strong></h4>
					<div class="content pr-0 pl-0">Vui lòng đăng kí tài khoản mua hàng để được tích điểm làm Member Vip</div>
				</div>
			</div>
		</div>
		<div class="policy-section">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-4">
						<div class="banner-footer-item border d-flex align-items-center mb-3 mb-md-5">
							<i class="fad fa-truck text-primary"></i>
							<div class="banner-footer-item-info">
								<p class="banner-footer-item-title">Miễn phí vận chuyển toàn quốc</p>
								<p class="banner-footer-item-des">Với Đơn Hàng Trên 500.000 đ</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-lg-4">
						<div class="banner-footer-item border d-flex align-items-center mb-3 mb-md-5">
							<i class="fad fa-user-headset text-danger"></i>
							<div class="banner-footer-item-info">
								<p class="banner-footer-item-title">Hỗ trợ 24/7</p>
								<p class="banner-footer-item-des"><span style="white-space:pre-wrap;">19001009</span></p>                   
							</div>
						</div>
					</div>
					<div class="col-12 col-lg-4">
						<div class="banner-footer-item border d-flex align-items-center mb-3 mb-md-5">
							<i class="fad fa-hand-holding-box text-success"></i>
							<div class="banner-footer-item-info">
								<p class="banner-footer-item-title">Miễn phí đổi hàng</p>
								<p class="banner-footer-item-des">Trong Vòng 03&nbsp;ngày</p>                    
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row wpb_row mt-4 pt-4 border-top">
		<div class="col-sm-12">
			<div class="content-editor h3-title ">
				<div class="content-info ">
					@if($related_product->count())
					<h3 class="title24 font-bold text-uppercase mb-4">Sản phẩm liên quan ({{$related_product->count()}})</h3>
					@else
					<h3 class="title24 font-bold text-uppercase mb-4">Không có sản phẩm nào liên quan</h3>
					@endif
				</div>
			</div>
			<div class="tabs-block block-element h3-tabs tab-style2 tab-ajax-off">
				<div class="vc_tta-panel-body">
					<div class="block-element  product-slider-view  slider filter- js-content-wrap">
						<div class="list-product-wrap">
							<div class="wrap-item smart-slider js-content-main clearfix group-navi " data-item="4" data-speed="" data-itemres="" data-prev="" data-next=""  data-pagination="" data-navigation="group-navi">
								@foreach($related_product as $product)
								 <div class="item">  
                                    <div class="product type-product status-publish has-post-thumbnail">
                                        <div class="item-product item-product-grid item-product-style2">
                                            <div class="product-thumb">
                                                <a href="{{URL::to('/san-pham/'.$product->product_slug)}}" class="product-thumb-link product-thumb-link zoomout-thumb" > 
                                                    <img  src="{{asset('/public/'.$product->product_image_path)}}" alt="{{$product->product_image}}" /> 
                                                    <img  src="{{asset('/public/'.$product->product_image_path)}}" alt="{{$product->product_image}}" /> 
                                                </a>       
                                                <div class="product-label">
                                                    @if($product->product_price_sale > 0)
                                                    <span class="sale">Sale</span>
                                                    @endif
                                                </div> 

                                                <div class="product-extra-link">
                                                    <button class="btn btn-primary product-quick-view quickview-link" data-bs-target="#quickview" data-bs-toggle="modal" data-id="{{$product->product_id}}"><i class="fal fa-shopping-bag"></i> <span>Mua nhanh</span></button>
                                                    <a href="{{URL::to('/san-pham/'.$product->product_slug)}}" class="btn btn-primary" data-id="{{$product->product_id}}"><span>Xem chi tiết</span> <i class="fal fa-long-arrow-right"></i></a>
                                                </div> 
                                            </div>
                                            <div class="product-info">
                                                <h3 class="title14 product-title">
                                                    <a title="{{$product->product_name}}" href="{{URL::to('/san-pham/'.$product->product_slug)}}">{{$product->product_name}}</a>
                                                </h3>
                                                <div class="product-price price variable">
                                                    @if($product->product_price_sale > 0)
                                                    <span class="price-sale">{{number_format($product->product_price)}} đ</span>
                                                    <span class="Price-amount">{{number_format($product->product_price_sale)}} đ</span>
                                                    <div class="product-label"><span class="sale-per">Giảm {{ number_format((float)100 - ($product->product_price_sale / $product->product_price * 100), 0, '.', '') }}%</span></div>
                                                    @else
                                                    <span class="Price-amount">{{number_format($product->product_price)}} đ</span>
                                                    @endif
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()