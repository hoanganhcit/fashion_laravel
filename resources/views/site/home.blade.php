@extends('index')
@section('title', 'Trang Chủ')
@section('layout')
{{-- <style>
    .header-page {
        position: absolute;
    }
</style> --}}
<div class="banner-slider banner-slider-3 ">
    <div class="wrap-item sv-slider  pagi-nav-style owl-carousel owl-theme" data-item="1" data-speed="5000" data-itemres="" data-animation="fade" data-navigation="" data-pagination="pagi-nav-style" data-prev="" data-next="">
        @foreach($slider_list as $key => $slide)
        <div class="item-slider  item-slider-3 ">
            <div class="banner-thumb">
                <a href="#">
                    <img width="1920" height="950" src="{{asset('public/uploads/sliders/'. $slide->slider_image)}}" class="attachment-full size-full" alt=""/>
                </a>
            </div>
            <div class="banner-info">
                <div class="container">
                    <div class="slider-content-text  text-left text-uppercase" data-animated="">
                        <h3 class="juliussans-font title60 text-uppercase black">{{$slide->slider_title}}</h3>
                        <p class="title24 text-upercase font-bold black">{{$slide->slider_subtitle}}</p>
                        <div class="button">
                            <a class="title14 more black" href="{{URL::to('/danh-muc/'. $slide->category_slug)}}">Xem Thêm</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div class="container">
    <div class="row wpb_row h3-banner-wrap mb150">
        <div class="col-sm-4 col-xs-12">
            <div class="banner-advs h3-banner-adv res767-mb50  zoom-image">
                <a href="{{URL::to('/danh-muc/ao-nam')}}" class="adv-thumb-link">    
                    <img width="372" height="506" src="{{asset('/public/frontend/images/h3-banner-1.png')}}"> 
                </a>            
                <div class="banner-info ">
                    <h3 class="title24 font-bold text-uppercase white">Thời trang nam</h3>
                    <p class="desc white">Bộ sưu tập thu đông</p>
                    <p><a class="title14 text-uppercase more white" href="{{URL::to('/danh-muc/ao-nam')}}">Xem thêm</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-xs-12">
            <div class="banner-advs h3-banner-adv h3-banner-adv2 res767-mb50  zoom-image">
                <a href="{{URL::to('/danh-muc/ao-nam')}}" class="adv-thumb-link">    
                    <img width="372" height="506" src="{{asset('/public/frontend/images/h3-banner-2.png')}}">   
                </a>            
                <div class="banner-info ">
                    <h3 class="title24 font-bold text-uppercase white">Khuyễn mãi 20%</h3>
                    <p class="desc white">Nhân dịp khai trương</p>
                    <p><a class="title14 text-uppercase more white" href="{{URL::to('/danh-muc/ao-nam')}}">Mua ngay</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-xs-12">
            <div class="banner-advs h3-banner-adv  zoom-image">
                <a href="{{URL::to('/danh-muc/ao-nu')}}" class="adv-thumb-link">    
                    <img width="372" height="506" src="{{asset('/public/frontend/images/h3-banner-3.png')}}">   
                </a>            
                <div class="banner-info ">
                    <h3 class="title24 font-bold text-uppercase white">Thời trang nữ</h3>
                    <p class="desc white">Mẫu mã theo xu hướng</p>
                    <p><a class="title14 text-uppercase more white" href="{{URL::to('/danh-muc/ao-nu')}}">Xem thêm</a></p>
                </div>
            </div>
        </div>
    </div>
    @include('site.home.product_new')
    <div class="banner-advs shop-banner  zoom-image line-scale mb-5">
        <a href="#" class="adv-thumb-link"> 
            <img width="1130" height="360" src="{{asset('/public/frontend/images/banner-shop-grid.jpg')}}"> 
        </a>            
        <div class="banner-info ">
            <h2 class="title60 juliussans-font text-uppercase black">Flash Sale</h2>
            <h3 class="title24 text-uppercase font-bold black">Thời trang nam</h3>
            <h3 class="title24 text-uppercase font-bold color2">Giảm giá lên tới 50%</h3>
            <p><a class="more title14 text-uppercase black" href="#">Mua ngay</a></p>
        </div>
    </div>
    @foreach($category_parent as $categoryParent)
        @include('site.home.all_product_category', ['categoryParent' => $categoryParent])
    @endforeach
    
</div>
@endsection()