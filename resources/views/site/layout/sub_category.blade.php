@if($categoryParent->childs->count())
<ul class="children">
	@foreach($categoryParent->childs as $categoryChild)
	<li class="cat-item">
		<a href="{{URL::to('danh-muc/'.$categoryChild->category_slug)}}">{{$categoryChild->category_name}}</a>
		@if($categoryChild->childs->count())
			@include('site.layout.sub_category', ['categoryParent' => $categoryChild])
		@endif
	</li>
	@endforeach
</ul>
@endif