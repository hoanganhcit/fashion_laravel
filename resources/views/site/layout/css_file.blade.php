
<!-- Custom fonts for this template-->
<link href="https://kit-pro.fontawesome.com/releases/v5.15.3/css/pro.min.css" rel="stylesheet" type="text/css"> 
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500&display=swap" rel="stylesheet">

<!-- Custom css for this template-->

<link href="{{asset('/public/frontend/css/bootstrap.min.css')}}" rel="stylesheet"> 
<link href="{{asset('/public/frontend/css/bootstrap-icons.css')}}" rel="stylesheet">
<link href="{{asset('/public/frontend/css/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{asset('/public/frontend/css/sweetalert.min.css')}}" rel="stylesheet">
<link href="{{asset('/public/frontend/css/alertify.min.css')}}" rel="stylesheet">
<link href="{{asset('/public/frontend/css/theme.css')}}" rel="stylesheet">
<link href="{{asset('/public/frontend/css/style.css')}}" rel="stylesheet">
<link href="{{asset('/public/frontend/css/responsives.css')}}" rel="stylesheet">