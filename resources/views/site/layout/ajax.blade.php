<script> 

    $(document).on("change","#sortBy", function () {
        var url = $(this).val();  
        if (url) { 
            window.location = url;
        }
        return false;
    });
    $(function () { 
        count_cart();
        mini_cart(); 
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
          return new bootstrap.Tooltip(tooltipTriggerEl)
        }); 
        $('.product-quick-view').on('click', quickview);
    });
    
    //  Xem nhanh AJAX
    function quickview(id){
        var product_id = $(this).data('id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:"{{url('/quickview')}}",
            method:"POST",
            dataType:"JSON",
            data:{product_id:product_id},
            success:function(data){
                $('#product_quickview_image').html(data.product_image);
                $('#product_quickview_gallery').html(data.product_gallery);
                $('#product_quickview_name').html(data.product_name);
                $('#product_name_hidden').html(data.product_name_hidden);
                $('#product_quickview_status').html(data.product_status);
                $('#product_quickview_price').html(data.product_price);
                $('#product_quickview_color').html(data.product_color);
                $('#product_quickview_size').html(data.product_size);
                $('#product_quickview_button').html(data.product_button); 
                
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                });
                detail_gallery();
            }
        });
    }

    $(document).on("click",".remove-this-item", function () {
        event.preventDefault();
        var url_del = $(this).data('url');
        $.ajax({
            url: url_del,
            method:"GET",
            success:function(data){
                $('.mini-cart-main-content').html(data);
                count_cart();
                mini_cart(); 
            }
        }); 
    });

    $(document).on("click",".destroy-order", function () {
        event.preventDefault();
        var order_id = $(this).attr('data-orderId');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '{{url('/destroy-order')}}',
            method:"POST",
            data:{order_id:order_id},
            success:function(data){
                window.location.href = "/my-account/orders";
            }
        }); 
    });

    $(document).on("click",".re-order", function () {
        event.preventDefault();
        var order_id = $(this).attr('data-orderId');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '{{url('/re-order')}}',
            method:"POST",
            data:{order_id:order_id},
            success:function(data){
                window.location.href = "/my-account/orders";
            }
        }); 
    });

    
    function detail_gallery() {
        if($('.detail-gallery').length>0){
            $('.detail-gallery').each(function(){
                var data=$(this).find(".carousel").data();
                var seff = $(this);
                if($(this).find(".carousel").length>0){
                    $(this).find(".carousel").jCarouselLite({
                        btnNext: $(this).find(".gallery-control .next"),
                        btnPrev: $(this).find(".gallery-control .prev"),
                        visible:data.visible,
                        vertical:data.vertical,
                    });
                }
                //Elevate Zoom              
                $.removeData($('.detail-gallery .mid img'), 'elevateZoom');//remove zoom instance from image
                $('.zoomContainer').remove();
                if($(window).width()>=768) {
                    $(this).find('.zoom-style1 .mid img').elevateZoom();
                    $(this).find('.zoom-style2 .mid img').elevateZoom({
                        scrollZoom : true
                    });
                    $(this).find('.zoom-style3 .mid img').elevateZoom({
                        zoomType: "lens",
                        lensShape: "square",
                        lensSize: 150,
                        borderSize:1,
                        containLensZoom:true,
                        responsive:true
                    });
                    $(this).find('.zoom-style4 .mid img').elevateZoom({
                        zoomType: "inner",
                        cursor: "crosshair",
                        zoomWindowFadeIn: 500,
                        zoomWindowFadeOut: 750
                    });
                }

                $(this).find(".carousel a").on('click',function(event) {
                    event.preventDefault();
                    $(this).parents('.detail-gallery').find(".carousel a").removeClass('active');
                    $(this).addClass('active');
                    var z_url =  $(this).find('img').attr("data-src");
                    var srcset =  $(this).find('img').attr("data-srcset");
                    $(this).parents('.detail-gallery').find(".mid img").attr("src", z_url);
                    $(this).parents('.detail-gallery').find(".mid img").attr("srcset", srcset);
                    $('.zoomWindow,.zoomLens').css('background-image','url("'+z_url+'")');
                    $.removeData($('.detail-gallery .mid img'), 'elevateZoom');//remove zoom instance from image
                    $('.zoomContainer').remove();
                    if($(window).width()>=768){
                        $(this).parents('.detail-gallery').find('.zoom-style1 .mid img').elevateZoom();
                        $(this).parents('.detail-gallery').find('.zoom-style2 .mid img').elevateZoom({
                            scrollZoom : true
                        });
                        $(this).parents('.detail-gallery').find('.zoom-style3 .mid img').elevateZoom({
                            zoomType: "lens",
                            lensShape: "square",
                            lensSize: 150,
                            borderSize:1,
                            containLensZoom:true,
                            responsive:true
                        });
                        $(this).parents('.detail-gallery').find('.zoom-style4 .mid img').elevateZoom({
                            zoomType: "inner",
                            cursor: "crosshair",
                            zoomWindowFadeIn: 500,
                            zoomWindowFadeOut: 750
                        });
                    }
                });
                $('input[name="variation_id"]').on('change',function(){
                    var z_url =  seff.find('.mid img').attr("src");
                    $('.zoomWindow,.zoomLens').css('background-image','url("'+z_url+'")');
                    $.removeData($('.detail-gallery .mid img'), 'elevateZoom');//remove zoom instance from image
                    $('.zoomContainer').remove();
                    $('.detail-gallery').find('.zoom-style1 .mid img').elevateZoom();
                    $('.detail-gallery').find('.zoom-style2 .mid img').elevateZoom({
                        scrollZoom : true
                    });
                    $('.detail-gallery').find('.zoom-style3 .mid img').elevateZoom({
                        zoomType: "lens",
                        lensShape: "square",
                        lensSize: 150,
                        borderSize:1,
                        containLensZoom:true,
                        responsive:true
                    });
                    $('.detail-gallery').find('.zoom-style4 .mid img').elevateZoom({
                        zoomType: "inner",
                        cursor: "crosshair",
                        zoomWindowFadeIn: 500,
                        zoomWindowFadeOut: 750
                    });
                })
                $('.image-lightbox').on('click',function(event){
                    event.preventDefault();
                    var gallerys = $(this).attr('data-gallery');
                    var gallerys_array = gallerys.split(',');
                    var data = [];
                    if(gallerys != ''){
                        for (var i = 0; i < gallerys_array.length; i++) {
                            if(gallerys_array[i] != ''){
                                data[i] = {};
                                data[i].src = gallerys_array[i];
                            }
                        };
                    }
                    $.fancybox.open(data);
                })
            });
        }
    }

    function count_cart() {
        $.ajax({
            url:'{{url('/count-cart')}}',
            method:"GET",
            success:function(data){
                $('.set-cart-number').html(data);
            }
        }); 
    };
    function mini_cart() {
        $.ajax({
            url:'{{url('/mini-cart')}}',
            method:"GET",
            success:function(data){
                $('.mini-cart-main-content').html(data);
            }
        }); 
    }; 

    //  Add to cart AJAX

    $(document).on("click",".single_add_to_cart_button", function () {
        event.preventDefault(); 
        var url = '{{url('/add-to-cart')}}';
        var quantity = $('#quantity_val').val();
        var product_name = $('#product_name').val();
        var product_id = $(this).data('id');
        var size = $('.size-item.active').data('value');
        var color = $('.color-item.active').data('value');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if(size == null && color == null) {
            alertify.set('notifier','position', 'top-right');
            alertify.error('Vui lòng chọn Size và Màu Sắc');
        }else if(size == null) {
            alertify.set('notifier','position', 'top-right');
            alertify.error('Vui lòng chọn Size');
        }else if(color == null) {
            alertify.set('notifier','position', 'top-right');
            alertify.error('Vui lòng chọn Màu Sắc');
        } else { 
            var seff = $(this);
            seff.find('i.fa-check').remove();
            seff.append('<i class="fal fa-spinner fa-spin"></i>');
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                crossDomain: true,
                data: {quantity:quantity, product_id:product_id, size:size, color:color},
                success: function (data) {
                    seff.find('.fa-spin').remove();
                    seff.append('<i class="fal fa-check"></i>'); 
                    alertify.set('notifier','position', 'top-right');
                    alertify.success('<div class="notifi_product"><i class="fal fa-check-circle"></i> <span><strong>"'+ product_name +'"</strong> đã được thêm vào giỏ hàng</span></div>');
                    count_cart();
                    mini_cart();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            }); 
        }
    });  
    

    $('.select_address').on('change', function() {
        var action = $(this).attr('id');
        var ma_id = $(this).val();
        var result = '';

        if(action == 'city'){
            result = 'province';
        }else {
            result = 'ward';
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '{{url('/select-delivery')}}',
            crossDomain: true,
            data:{action:action, ma_id:ma_id},
            success: function (data) { 
                $('#'+result).html(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    // function fee_feeship() {
    //     $.ajax({
    //         url:'{{url('/fee-feeship')}}',
    //         method:"GET",
    //         success:function(data){
    //             $('#ship_fee').html(data);
    //         }
    //     }); 
    // };

    // $('.ward').on('change', function() {
    //     var matp = $('.city').val();
    //     var maqh = $('.province').val();
    //     var xaid = $('.ward').val();

    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });

    //     $.ajax({
    //         type: "POST",
    //         url: '{{url('/calculator-fee')}}',
    //         crossDomain: true,
    //         data:{matp:matp, maqh:maqh, xaid:xaid},
    //         success: function (data) {
    //             fee_feeship();
    //         },
    //         error: function (data) {
    //             console.log('Error:', data);
    //         }
    //     }); 

    // });

    function result_product_by_category(parent_id) {
        $.ajax({
            url:'{{url('/render-product-by-category')}}',
            method:"GET",
            success:function(data){
                $('#category-' + parent_id).html(data);
            }
        }); 
    }; 

    $(document).on("click",".click_product_by_category", function () {
        event.preventDefault(); 
        var category_id = $(this).data('id');
        var parent_id = $(this).data('parentid');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var seff = $('#waiting-' + parent_id);
        seff.find('.loading').remove();
        seff.append('<div class="loading"><img src="{!! asset('public/frontend/images/loader.svg') !!}" class="fa-spin"></div>');
        $.ajax({
            url:"{{url('/get-product-by-category')}}",
            method:"POST",
            dataType:"JSON",
            data:{category_id:category_id},
            success:function(data){
                $('.loading').remove();
                result_product_by_category(parent_id);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });

</script>