
<div id="header" class="header-page" style="min-height: 120px;">
    <div class="container-fluid">
        <div class="vc_row wpb_row header-row header-s1 header-s2 header-s3 vc_rtl-columns-reverse vc_row-o-equal-height vc_row-o-content-middle vc_row-flex header_row" style="">
            <div class="wpb_wrapper">
                <div class="logo ">
                    <div class="text-logo">
                        <h1 class="color">             
                            <a href="{{URL::to('/')}}">
                                <img class="alignnone wp-image-991" src="{{asset('/public/frontend/images/logo.png')}}" alt="" height="50px">
                            </a>
                        </h1> 
                    </div>
                </div>
                <nav class="main-nav  main-nav2 menu-sticky-on"> 
                    <ul id="menu-main-menu" class="list-none">  
                        @foreach($category_parent as $categoryParent)
                        <li class="main-menu-item menu-item-even menu-item-has-children">
                            <a href="{{URL::to('danh-muc/'.$categoryParent->category_slug)}}" class="menu-link main-menu-link">{{$categoryParent->category_name}}</a>
                            @include('site.layout.sub_menu', ['categoryParent' => $categoryParent])
                        </li>
                        @endforeach 
                        <li class="main-menu-item menu-item-even">
                            <a href="#">Tin tức</a>
                        </li> 
                    </ul>
                </nav>
                <div class="block-end">
                    <div class="block-element block-search-element  search-icon">
                        <a class="s-icon" href="javascript:;">
                            <i class="fal fa-search"></i>
                            <i class="fal fa-times"></i>
                        </a>
                        <div class="search-form-wrap">
                            <form class="search-form  search-icon live-search-on typeahead" action="">
                                <input class="animated fadeInUp search-input" name="s" type="text" placeholder="Nhập tên sản phẩm ...">
                            </form>
                        </div>
                    </div>
                    <div class="mini-cart-box  mini-cart1 aside-box">
                        <a class="mini-cart-link" href="#">
                            <i class="fal fa-shopping-cart"></i>
                            @if(Session::get('cart'))
                            <span class="mini-cart-number set-cart-number"></span>
                            @else
                            <span class="mini-cart-number set-cart-number">0</span>
                            @endif
                        </a>
                        <div class="mini-cart-content dropdown-list text-left">
                            <div class="d-flex justify-between">
                                <span class="title-cart">Giỏ Hàng</span>
                                <span class="close-minicart"><i class="fal fa-times"></i></span>
                            </div>
                            <span class="textCartSide">Bạn đang có 
                                @if(Session::get('cart'))
                                <b class="set-cart-number"></b> 
                                @else
                                <b class="set-cart-number">0</b>
                                @endif
                            sản phẩm trong giỏ hàng.</span>
                            <div class="mini-cart-main-content">
                                @if(!Session::get('cart'))
                                <div class="mini-cart-empty">
                                    <i class="fal fa-shopping-cart title120 empty-icon"></i>
                                    <p class="mt-4">Hiện chưa có sản phẩm</p>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="cart-overlay"></div>    
                    </div>
                    <div class="account-manager dropdown-box "> 
                        @php
                        $customer = Session::get('customer_id');
                        $customer_name = Session::get('customer_name');
                        @endphp
                        @if($customer)
                        <a href="#" class="dropdown dropdown-username" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ $customer_name }} <span class="fal fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ URL::to('/my-account/orders') }}"><i class="fal fa-clipboard-list"></i> Đơn hàng</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/my-account/edit-account') }}"><i class="fal fa-user"></i> Tài khoản</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/customer/logout') }}"><i class="fal fa-sign-out"></i> Đăng xuất</a>
                            </li>
                        </ul>
                        @else
                        <a class="dropdown icon-dropdown" href="#"><i class="fal fa-user"></i></a>    
                        <ul class="dropdown-menu">
                            <li><a href="{{ URL::to('customer/login') }}">Đăng Nhập Thành Viên</a></li> 
                        </ul>
                        @endif 
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
    </div>
</div>