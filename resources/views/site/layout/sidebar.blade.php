<div class="col-md-3 col-sm-4 col-xs-12">
	<div class="sidebar sidebar-right">
		<div id="s7upf_category_list-2" class="sidebar-widget widget widget_s7upf_category_list">
			<h3 class="widget-title">Danh Mục Sản Phẩm</h3>
			<ul class="list-none">	
				@foreach($category_parent as $categoryParent)
					<li class="cat-item">
						<a href="{{URL::to('danh-muc/'.$categoryParent->category_slug)}}">{{$categoryParent->category_name}}</a>
						@include('site.layout.sub_category', ['categoryParent' => $categoryParent])
					</li>
				@endforeach
			</ul>
		</div>
		<div id="" class="sidebar-widget widget">
			<h3 class="widget-title">Khoảng giá</h3>                            
			<ul class="tawcvs-swatches price-filter-type-label">
				<li>
					<a class="{{ Request::get('price') == '200000' ? ' active ' : '' }}" href="{{request()->fullUrlWithQuery(['price' => '200000'])}}">
						<i class="fad fa-square"></i>
						<label>Dưới 200,000</label>
					</a>
					<a class="{{ Request::get('price') == '200000 - 500000' ? ' active ' : '' }}" href="{{request()->fullUrlWithQuery(['price' => '200000 - 500000'])}}">
						<i class="fad fa-square"></i>
						<label>Từ 200,000 - 500,000</label>
					</a>
					<a class="{{ Request::get('price') == '500000 - 1000000' ? ' active ' : '' }}" href="{{request()->fullUrlWithQuery(['price' => '500000 - 1000000'])}}">
						<i class="fad fa-square"></i>
						<label>Từ 500,000 - 1,000,000</label>
					</a>
					<a class="{{ Request::get('price') == '1000000' ? ' active ' : '' }}" href="{{request()->fullUrlWithQuery(['price' => '1000000'])}}">
						<i class="fad fa-square"></i>
						<label>Trên 1,000,000</label>
					</a>
				</li>                               
			</ul>
		</div>
		@if($brand_product->count())
		<div id="-1" class="sidebar-widget widget">
			<h3 class="widget-title">Thương Hiệu</h3>
			<ul class="list-none">
				@foreach($brand_product as $key => $brand)
					<li class="cat-item">
						<a class="" href="{{URL::to('thuong-hieu/'.$brand->brand_slug)}}">
							<span>{{$brand->brand_name}}</span>
						</a>
					</li>
				@endforeach                       
			</ul>
		</div>	
		@endif	 	
	</div>
</div>