@if($categoryParent->childs->count())
<div class="mega-menu sub-menu">
	<ul>
		@foreach($categoryParent->childs as $categoryChild)
		<li class="sub-menu-item">
			<a href="{{URL::to('danh-muc/'.$categoryChild->category_slug)}}" class="menu-link sub-menu-link">{{$categoryChild->category_name}}</a>
			@if($categoryChild->childs->count())
			@include('site.layout.sub_menu', ['categoryParent' => $categoryChild])
			@endif
		</li>
		@endforeach
	</ul>
	<div class="mega-product-menu">
		@foreach($categoryParent->products->take(3) as $product)
			<div class="item">  
				<div class="product type-product status-publish has-post-thumbnail">
					<div class="item-product item-product-grid item-product-style2">
						<div class="product-thumb">
							<a href="{{URL::to('/san-pham/'.$product->product_slug)}}" class="product-thumb-link product-thumb-link" > 
								<img  src="{{asset('/public/'.$product->product_image_path)}}" alt="{{$product->product_image}}" /> 
							</a> 
						</div>
						<div class="product-info">
							<h3 class="title14 product-title">
								<a title="{{$product->product_name}}" href="{{URL::to('/san-pham/'.$product->product_slug)}}">{{str_limit($product->product_name, 13)}}</a>
							</h3> 
						</div>
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>
@endif