<div id="footer" class="footer-page">
    <div class="vc_row row footer-newsletter">
        <div class="col-sm-12">
            <div class="block-element newsletter-form text-center  sv-mailchimp-form" data-placeholder="Nhập địa chỉ email của bạn" data-submit="Đăng ký">
                <h3 class="title24 font-bold text-uppercase">Nhận thông tin khuyễn mãi</h3>
                <p class="mail-desc desc">Đăng ký để nhận thông tin cập nhật hàng mới về, ưu đãi đặc biệt và thông tin giảm giá khác.</p>
                <div class="form-newsletter">
                    <form id="" class="mc4wp-form mc4wp-form-482" method="post" data-id="" data-name="Mailchimp">
                        <div class="mc4wp-form-fields">
                            <input type="email" name="EMAIL" placeholder="Nhập địa chỉ email của bạn" required="">
                            <div class="submit-form">
                                <input type="submit" value="Đăng ký">
                            </div>
                        </div>
                        <input type="hidden" name="_mc4wp_timestamp" value="1626961490">
                        <input type="hidden" name="_mc4wp_form_id" value="482">
                        <input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1">
                        <div class="mc4wp-response"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row-full-width vc_clearfix"></div>

    <div class="container">
        <div class="vc_row row footer-top pt50">
            <div class="col-sm-12 col-md-3 col-xs-12">
                <div class="logo footer-logo">
                    <div class="text-logo">
                        <h1 class="color">             
                            <a href="{{URL::to('/')}}">
                                <img class="alignnone size-full wp-image-991" src="{{asset('public/frontend/images/logo.png')}}" alt=""></strong>            
                            </a>
                        </h1>     
                    </div>
                </div>
                <div class="content-editor footer-contact ">
                    <div class="content-info ">
                        <ul class="list-none">
                            <li>
                                <p class="desc">666 Ngô Quyền - P.An Hải Bắc - Q. Sơn Trà - TP. Đà Nẵng
                                </p>
                            </li>
                            <li><a href="tel:+84989177556">037 4466 344</a></li>
                            <li><a href="tel:+844378311160">035 9812 063</a></li>
                            <li><a href="mailto:hoanganh94cit@gmail.com">hoanganh94cit@gmail.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3 col-xs-12">
                <div class="content-editor footer-box ">
                    <div class="content-info ">
                        <h3 class="title14 font-bold text-uppercase">Thông tin thêm</h3>
                        <ul class="list-none">
                            <li><a href="#">Về chúng tôi</a></li>
                            <li><a href="#">Liên Hệ</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Điều khoản & Điều kiện</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3 col-xs-12">
                <div class="content-editor footer-box ">
                    <div class="content-info ">
                        <h3 class="title14 font-bold text-uppercase">Dịch Vụ</h3>
                        <ul class="list-none">
                            <li><a href="#">Chính sách vận chuyển</a></li>
                            <li><a href="#">Thông tin giao hàng</a></li>
                            <li><a href="#">F.A.Q.'s</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3 col-xs-12">
                <div class="content-editor footer-box ">
                    <div class="content-info ">
                        <h3 class="title14 font-bold text-uppercase">Liên kết </h3>
                        <ul class="list-none">
                            <li><a class="float" href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-f"></i> <span class="text-hidden">Facebook</span></a></li>
                            <li><a class="float" href="https://mail.google.com/mail/u/0/#inbox" target="_blank"><i class="fab fa-google-plus-g"></i> <span class="text-hidden">Google</span></a></li>
                            <li><a class="float" href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram"></i> <span class="text-hidden">Instagram</span></a></li>
                            <li><a class="float" href="https://www.twitter.com" target="_blank"><i class="fab fa-twitter"></i> <span class="text-hidden">Twitter</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row row footer-bottom ">
            <div class="col-sm-8 col-md-8 col-xs-12">
                <div class="content-editor footer-copyright">
                    <div class="content-info ">
                        <p class="desc">Copyright <a class="color2" href="{{URL::to('/')}}">HT Store</a> 2021.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 col-xs-12">
                <div class="content-editor footer-payment text-right ">
                    <div class="content-info ">
                        <ul class="list-inline-block">
                            <li><a class="float" href="#"><img class="alignnone size-full wp-image-1092" src="https://vollemobel.7uptheme.net/wp-content/uploads/2018/09/paymethod1.jpg" alt="" width="100" height="40"></a></li>
                            <li><a class="float" href="#"><img class="alignnone size-full wp-image-1093" src="https://vollemobel.7uptheme.net/wp-content/uploads/2018/09/paymethod2.jpg" alt="" width="100" height="40"></a></li>
                            <li><a class="float" href="#"><img class="alignnone size-full wp-image-1094" src="https://vollemobel.7uptheme.net/wp-content/uploads/2018/09/paymethod3.jpg" alt="" width="100" height="40"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>