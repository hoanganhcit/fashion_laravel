<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>{{$meta_title}}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{{$meta_keyworks}}">
    <meta name="description" content="{{$meta_desc}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta property="og:url" content=""/>
    <meta property="og:title" content=""/>
    <meta property="og:description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/public/frontend/images/favicon.png')}}">
    <link rel="canonical" href="{{$meta_canonical}}">
    @include('site.layout.css_file') 
</head>
<body class=""> 
    <div class="modal quickview-modal fade" id="quickview" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <span data-bs-dismiss="modal" class="close-modal">&times;</span>
                <div class="product-detail main-detail-full">
                    <div class="row">
                        <div class="col-md-7 col-sm-12 col-xs-12">
                            <div class="detail-gallery has-gallery">
                                <div class="wrap-detail-gallery images zoom-style3">
                                    <div class="mid woocommerce-product-gallery__image image-lightbox true" data-gallery="" id="product_quickview_image">
                                    </div>
                                    <div class="gallery-control true">
                                        <a href="#" class="prev"><i class="fal fa-arrow-down"></i></a>
                                        <div class="carousel" data-visible="4" data-vertical="true">
                                            <ul class="list-none" id="product_quickview_gallery"></ul>
                                        </div>
                                        <a href="#" class="next"><i class="fal fa-arrow-up"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="summary entry-summary detail-info">
                                <h2 class="product-title title30 font-bold" id="product_quickview_name"></h2>
                                <div class="product-price" id="product_quickview_price"></div>
                                <div class="woocommerce-product-details__short-description">
                                    <div class="req clearfix" id="product_quickview_status"></div>
                                    <div class="size mb-3" >
                                        <div class="d-flex justify-content-between align-items-baseline mb-2 mt-3 mt-md-0">
                                            <label>Màu sắc</label>
                                        </div>
                                        <div class="list-color" id="product_quickview_color"></div>
                                    </div>
                                    <div class="size mb-3" >
                                        <div class="d-flex justify-content-between align-items-baseline mb-2 mt-3 mt-md-0">
                                            <label>Size</label>
                                        </div>
                                        <div class="list-size" id="product_quickview_size"></div>
                                    </div>
                                    <div class="qty_row">
                                        <div class="detail-qty product-qty info-qty border radius6">
                                            <a href="javascript:;" class="qty-down"><i class="fal fa-minus-square" aria-hidden="true"></i></a>
                                            <input type="text" id="quantity" name="quantity" value="1" title="Qty" class="input-text text qty qty-val" />
                                            <a href="javascript:;" class="qty-up"><i class="fal fa-plus-square" aria-hidden="true"></i></a>
                                            <input type="hidden" name="quantity" id="quantity_val" value="1">
                                        </div>
                                        <div id="product_name_hidden"></div>
                                        <div id="product_quickview_button"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="scroll-top dark">
        <span class="text-bttop">Về đầu trang</span>
        <i class="fal fa-long-arrow-right"></i>
    </a>
    <div class="wrap">
        @include('site.layout.header')
        <div id="main-content">
            @yield('layout')
        </div>
        @include('site.layout.footer')
    </div>
    @include('site.layout.js_file')
    @include('site.layout.ajax')
</body> 
<script>
    jQuery(document).ready(function($) {
        var base_url = window.location.origin;
        var engine = new Bloodhound({
            remote: {
                url: '{{url('/product/find?q=%QUERY%')}}',
                wildcard: '%QUERY%'
            },
            datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });

        $(".search-input").typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            source: engine.ttAdapter(),
            name: 'ProductList',
            display: 'none',
            templates: {
                empty: [
                '<div class="list-group search-results-dropdown"><div class="list-group-item">Không có kết quả phù hợp.</div></div>'
                ],
                header: [
                '<div class="list-group search-results-dropdown">'
                ],
                suggestion: function (data) {
                    return '<div class="item-search-pro"><div class="search-ajax-thumb product-thumb"><a href="san-pham/' + data.product_slug + '" class="product-thumb-link"><img width="50" height="50" src="'+ base_url +'/public' + data.product_image_path + '" class="attachment-50x50 size-50x50 wp-post-image" alt=""></a></div><div class="search-ajax-title"><h3 class="title14"><a href="'+ base_url +'/san-pham/' + data.product_slug + '">' + data.product_name + '</a></h3></div><div class="product-price price variable"><span class="Price-amount">' + parseFloat(data.product_price) + '  đ</span></div></div>'
                }
            }
        });
    });
</script>
</html>