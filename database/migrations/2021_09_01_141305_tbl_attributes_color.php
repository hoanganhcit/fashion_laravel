<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblAttributesColor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_attributes_color', function (Blueprint $table) {
            $table->increments('attr_id');
            $table->integer('product_id');
            $table->string('attributes_color_type');
            $table->string('attributes_color_value')->nullable();
            $table->string('attributes_color_qty')->nullable();
            $table->string('attributes_color_price')->nullable();
            $table->string('attributes_color_image')->nullable();
            $table->string('attributes_color_image_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_attributes_color');
    }
}
