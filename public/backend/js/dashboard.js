$(function () {

    // Dashboard chart colors
    const body_styles = window.getComputedStyle(document.body);
    const colors = {
        primary: $.trim(body_styles.getPropertyValue('--bs-primary')),
        secondary: $.trim(body_styles.getPropertyValue('--bs-secondary')),
        info: $.trim(body_styles.getPropertyValue('--bs-info')),
        success: $.trim(body_styles.getPropertyValue('--bs-success')),
        danger: $.trim(body_styles.getPropertyValue('--bs-danger')),
        warning: $.trim(body_styles.getPropertyValue('--bs-warning')),
        light: $.trim(body_styles.getPropertyValue('--bs-light')),
        dark: $.trim(body_styles.getPropertyValue('--bs-dark')),
        blue: $.trim(body_styles.getPropertyValue('--bs-blue')),
        indigo: $.trim(body_styles.getPropertyValue('--bs-indigo')),
        purple: $.trim(body_styles.getPropertyValue('--bs-purple')),
        pink: $.trim(body_styles.getPropertyValue('--bs-pink')),
        red: $.trim(body_styles.getPropertyValue('--bs-red')),
        orange: $.trim(body_styles.getPropertyValue('--bs-orange')),
        yellow: $.trim(body_styles.getPropertyValue('--bs-yellow')),
        green: $.trim(body_styles.getPropertyValue('--bs-green')),
        teal: $.trim(body_styles.getPropertyValue('--bs-teal')),
        cyan: $.trim(body_styles.getPropertyValue('--bs-cyan')),
        chartTextColor: $('body').hasClass('dark') ? '#6c6c6c' : '#b8b8b8',
        chartBorderColor: $('body').hasClass('dark') ? '#444444' : '#ededed',
    };

    $(document).on('click', '.select-all', function () {
        const that = $(this),
            target = $(that.data('select-all-target')),
            checkbox = target.find('input[type="checkbox"]');

        if (that.prop('checked')) {
            checkbox.closest('tr').addClass('tr-selected');
            checkbox.prop('checked', true);
        } else {
            checkbox.closest('tr').removeClass('tr-selected');
            checkbox.prop('checked', false);
        }
    });

    $(document).on('click', '#recent-products input[type="checkbox"]', function () {
        const that = $(this);

        if (that.prop('checked')) {
            that.closest('tr').addClass('tr-selected');
        } else {
            that.closest('tr').removeClass('tr-selected');
        }
    });
    $('.menu-sidebar li a').on('click',function(event){
        $('.menu-sidebar li a').removeClass('active');
        $(this).addClass('active');
    });

    function totalSales() {
        if ($('#total-sales').length) {
            const options = {
                series: [{
                    data: [25, 66, 41, 89, 63, 30, 50]
                }],
                chart: {
                    type: 'line',
                    width: 100,
                    height: 35,
                    sparkline: {
                        enabled: true
                    }
                },
                theme: {
                    mode: $('body').hasClass('dark') ? 'dark' : 'light',
                },
                colors: [colors.indigo],
                stroke: {
                    width: 4,
                    curve: 'smooth',
                },
                tooltip: {
                    fixed: {
                        enabled: false
                    },
                    x: {
                        show: false
                    },
                    y: {
                        title: {
                            formatter: function (seriesName) {
                                return ''
                            }
                        }
                    },
                    marker: {
                        show: false
                    }
                }
            };

            new ApexCharts(document.querySelector("#total-sales"), options).render();
        }
    }

    totalSales();

    function totalOrders() {
        if ($('#total-orders').length) {
            const options = {
                series: [{
                    data: [25, 66, 41, 89, 63, 30, 50]
                }],
                chart: {
                    type: 'line',
                    width: 100,
                    height: 35,
                    sparkline: {
                        enabled: true
                    }
                },
                theme: {
                    mode: $('body').hasClass('dark') ? 'dark' : 'light',
                },
                colors: [colors.pink],
                stroke: {
                    width: 4,
                    curve: 'smooth',
                },
                tooltip: {
                    fixed: {
                        enabled: false
                    },
                    x: {
                        show: false
                    },
                    y: {
                        title: {
                            formatter: function (seriesName) {
                                return ''
                            }
                        }
                    },
                    marker: {
                        show: false
                    }
                }
            };

            new ApexCharts(document.querySelector("#total-orders"), options).render();
        }
    }

    totalOrders();

    function customerRating() {
        if ($('#customer-rating').length) {
            const options = {
                series: [{
                    name: 'Rate',
                    data: [25, 66, 41, 89, 63, 25, 44, 12, 36]
                }],
                chart: {
                    type: 'line',
                    height: 50,
                    sparkline: {
                        enabled: true
                    }
                },
                stroke: {
                    width: 4,
                    curve: 'smooth',
                },
                theme: {
                    mode: $('body').hasClass('dark') ? 'dark' : 'light',
                },
                colors: [colors.success],
                tooltip: {
                    fixed: {
                        enabled: false
                    },
                    x: {
                        show: false
                    },
                    y: {
                        title: {
                            formatter: function (seriesName) {
                                return seriesName;
                            }
                        }
                    },
                    marker: {
                        show: false
                    }
                }
            };

            new ApexCharts(document.querySelector("#customer-rating"), options).render();
        }
    }

    customerRating();

    function salesChart() {

        let listDay = $('#sales-chart').attr('data-list-day');
        listDay = JSON.parse(listDay);  

        let revenue = $('#sales-chart').attr('data-revenue'); 
        revenue = JSON.parse(revenue);  

        let count_order = $('#sales-chart').attr('data-count'); 
        count_order = JSON.parse(count_order); 
        
        if ($('#sales-chart').length) {
            const options = {
                series: [
                    {
                        name: "Doanh số:",
                        type: 'column',
                        data: revenue
                    },
                    {
                        name: "Số đơn hàng:",
                        type: 'line',
                        data: count_order
                    }
                ],
                theme: {
                    mode: $('body').hasClass('dark') ? 'dark' : 'light',
                },
                chart: {
                    height: 350,
                    type: 'line',
                    foreColor: colors.chartTextColor,
                    zoom: {
                        enabled: false
                    },
                    toolbar: {
                        show: false
                    }
                },
                dataLabels: {
                    enabled: true,
                    enabledOnSeries: [1]
                },
                colors: [colors.success, colors.primary],
                stroke: {
                    width: [0, 4],
                    curve: 'smooth',
                },
                legend: {
                    show: false
                },
                markers: {
                    size: 0,
                    hover: {
                        sizeOffset: 6
                    }
                },
                labels: listDay,
                xaxis: {
                  // type: 'datetime'
                },
                yaxis: [{
                    title: {
                        text: "",
                    },

                }, {
                    opposite: true,
                    title: {
                        text: ""
                    }
                }],
                tooltip: {
                    y: [
                        {
                            title: {
                                formatter: function (val) {
                                    return val
                                }
                            }
                        },
                        {
                            title: {
                                formatter: function (val) {
                                    return val
                                }
                            }
                        },
                        {
                            title: {
                                formatter: function (val) {
                                    return val;
                                }
                            }
                        }
                    ]
                },
                grid: {
                    borderColor: colors.chartBorderColor,
                }
            };

            new ApexCharts(document.querySelector("#sales-chart"), options).render();
        }
    }

    salesChart(); 

    if ($('.summary-cards').length) {
        $('.summary-cards').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 1500,
            rtl: $('body').hasClass('rtl') ? true : false
        });
    }
});



function ChangeToSlug()
{
    var slug;

    slug = document.getElementById("name").value;
    slug = slug.toLowerCase();
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');

    slug = slug.replace(/ /gi, "-");
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    document.getElementById('convert_slug').value = slug;
}

/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */

$(function () {
    $('#upload').on('change', function () {
        $('.image-area').show();
        readURL(input);
    });
}); 

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
/*  ==========================================
    SHOW UPLOADED IMAGE NAME
* ========================================== */

$(document).ready(function () {

    $(".tagsinput-example").tagsinput('items');

    var input = document.getElementById( 'upload' );
    var infoArea = document.getElementById( 'upload-label' );

    input.addEventListener( 'change', showFileName );
    function showFileName( event ) {
        var input = event.srcElement;
        var fileName = input.files[0].name;
        infoArea.textContent = 'File name: ' + fileName;
    }


});



$(document).ready(function(){
    $(".delete-item").click(function(){
        var current_object = $(this);
        swal({
            title: "Thông báo !",
            text: "Bạn có chắc muốn xoá?",
            type: "error",
            icon: "danger",
            showCancelButton: true,
            dangerMode: true,
            cancelButtonClass: '#DD6B55',
            confirmButtonColor: '#dc3545',
            confirmButtonText: 'Xoá',
            cancelButtonText: 'Huỷ bỏ',
        },function (result) {
            if (result) {
                var action = current_object.attr('data-action');
                var token = jQuery('meta[name="csrf-token"]').attr('content');
                var id = current_object.attr('data-id');

                $('.content').html("<form class='form-inline remove-form' method='get' action='"+action+"'></form>");
                $('.content').find('.remove-form').append('<input name="_method" type="hidden" value="delete">');
                $('.content').find('.remove-form').append('<input name="_token" type="hidden" value="'+token+'">');
                $('.content').find('.remove-form').append('<input name="id" type="hidden" value="'+id+'">');
                $('.content').find('.remove-form').submit();
            }
        });
    });
});




$(document).ready(function(){
    $('.edit_feeship').click(function () {
        $(this).hide();
        $(this).parent().find('.update_feeship').show();
    });
});


$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#gallery-product").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $('.preview-gallery').append("<span class=\"gallery-item\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\"><i class=\"fad fa-trash text-danger\"></i></span>" +
            "</span>");
          $(".remove").click(function(){
            $(this).parent(".gallery-item").remove();
          });
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Trình duyệt của bạn không hỗ trợ API tệp")
  }
});
 