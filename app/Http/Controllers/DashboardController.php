<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\ProductModel;
use App\Customer;
use App\Order;
use App\OrderDetail;
use App\Shipping;
use App\Http\Requests;
use App\Components\Date;
use App\Traits\ArrayOrderStrait;
use Illuminate\Support\Facades\Redirect;
session_start();

class DashboardController extends Controller
{

    use ArrayOrderStrait;
    private $order;
    public function __construct(Order $order) {
        $this->order = $order;
    }
    public function CheckLogin() {
        if(Session::get('admin_id')){
            $admin_id = Session::get('admin_id');
        }else{
            $admin_id = Auth::id();
        }
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        } 
    }
    public function overview()
    {
        $this->CheckLogin();

        $orders = Order::where('order_status',2)->sum('order_total');
        $order_new = Order::where('order_status',0)->get();
        $products_total = ProductModel::count();
        $customer_total = Customer::count();   

        // Biểu đồ thống kê doanh số bán hàng trong tháng

        $now = Carbon::now()->startOfWeek()->subDay(1);
        $listDay = Date::getListDayInWeek($now);
        $dateS = Carbon::now()->startOfWeek()->format('Y-m-d');
        $dateE = Carbon::now()->endOfWeek()->format('Y-m-d');
        $total_order = Order::where('order_status',2)->whereBetween('order_date',[$dateS,$dateE])->sum('order_total'); 
        $arrayRevenueOrderMonth = $this->arrayRevenueOrderGet($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);
        $arrayCountOrderMonth = $this->arrayCountOrderCount($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);


        if(isset($_GET['preview'])){

            $preview = $_GET['preview'];

            switch ($preview) { 
                case 'this_week':
                    $week = Carbon::now()->startOfWeek()->subDay(1);
                    $listDay = Date::getListDayInWeek($week); 
                    $dateS = Carbon::now()->startOfWeek()->format('Y-m-d');
                    $dateE = Carbon::now()->endOfWeek()->format('Y-m-d');
                    $total_order = Order::where('order_status',2)->whereBetween('order_date',[$dateS,$dateE])->sum('order_total'); 
                    $arrayRevenueOrderMonth = $this->arrayRevenueOrderGet($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);
                    $arrayCountOrderMonth = $this->arrayCountOrderCount($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);

                break; 

                case 'last_week':
                    $week = Carbon::now('Asia/Ho_Chi_Minh')->subWeek()->startOfWeek()->subDay(1);
                    $listDay = Date::getListDayInWeek($week); 
                    $dateS = Carbon::now()->subWeek()->startOfWeek()->format('Y-m-d');
                    $dateE = Carbon::now()->subWeek()->endOfWeek()->format('Y-m-d'); 
                    $total_order = Order::where('order_status',2)->whereBetween('order_date',[$dateS,$dateE])->sum('order_total'); 
                    $arrayRevenueOrderMonth = $this->arrayRevenueOrderGet($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);
                    $arrayCountOrderMonth = $this->arrayCountOrderCount($listDay = $listDay, $dateS = $dateS, $dateE = $dateE); 
                    
                break; 

                case 'this_month':
                    $this_month = Carbon::now()->month;
                    $listDay = Date::getListDayInMonth($this_month);
                    $dateS = Carbon::now()->startOfMonth()->format('Y-m-d');
                    $dateE = Carbon::now()->endOfMonth()->format('Y-m-d'); 
                    $total_order = Order::where('order_status',2)->whereBetween('order_date',[$dateS,$dateE])->sum('order_total');
                    $arrayRevenueOrderMonth = $this->arrayRevenueOrderGet($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);
                    $arrayCountOrderMonth = $this->arrayCountOrderCount($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);
                break;

                case 'last_month':
                    $this_month = Carbon::now()->startOfMonth()->subMonth()->format('m');
                    $listDay = Date::getListDayInMonth($this_month); 
                    $dateS = Carbon::now()->subMonth()->startOfMonth()->format('Y-m-d');
                    $dateE = Carbon::now()->subMonth()->endOfMonth()->format('Y-m-d'); 
                    $total_order = Order::where('order_status',2)->whereBetween('order_date',[$dateS,$dateE])->sum('order_total');
                    $arrayRevenueOrderMonth = $this->arrayRevenueOrderGet($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);
                    $arrayCountOrderMonth = $this->arrayCountOrderCount($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);

                break;

                case 'default':
                    $this_month = Carbon::now()->month;
                    $listDay = Date::getListDayInMonth($this_month);
                    $dateS = Carbon::now()->startOfMonth()->format('Y-m-d');
                    $dateE = Carbon::now()->endOfMonth()->format('Y-m-d'); 
                    $total_order = Order::where('order_status',2)->whereBetween('order_date',[$dateS,$dateE])->sum('order_total');
                    $arrayRevenueOrderMonth = $this->arrayRevenueOrderGet($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);
                    $arrayCountOrderMonth = $this->arrayCountOrderCount($listDay = $listDay, $dateS = $dateS, $dateE = $dateE);
                break;
            }

        }

        // Biểu đồ thống kê sản phẩm bán chạy trong tháng

        $best_sellers = ProductModel::orderBy('product_sold', 'desc')->limit(5)->get();

        // $year = Date::getListDayInYear();
        // dd($year);

        // dd(json_encode($product_name, JSON_UNESCAPED_UNICODE));
        return view('admin.template.overview', compact('orders','order_new','products_total','customer_total','total_order','listDay','arrayRevenueOrderMonth', 'arrayCountOrderMonth','best_sellers'));
    } 

}
