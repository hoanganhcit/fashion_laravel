<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\ProductModel;
use App\AttributesProduct;
use App\AttributesProductValue;
use App\AttributesColorProduct;
use Illuminate\Support\Str;
use App\Exports\ExcelExports;
use App\Imports\ExcelImports;
use Session;
use Auth;
use Storage;
use App\Traits\UploadImageStrait;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect; 
session_start();

class AttributesController extends Controller
{
    
    //  Thuộc tính sản phẩm
	public function attributes_value() {
		$product_id = Session::get('product_id');
		$attributes_id = Session::get('attributes_id');
		$attributes_value = AttributesProductValue::where('product_id', $product_id)->get();
		$output = ''; 
		if($attributes_value) {
			foreach($attributes_value as $value) {
				$output .= '<div class="attributes_item--child pr-150 row mb-2 justify-between">';
				$output .= '<div class="col-md-4 col-xs-12">';
				$output .= '<input type="text" class="form-control attr_name" placeholder="Nhập thuộc tính , ví dụ: M, L, Đen,Đỏ" name="value" value="'.$value->attributes_value.'">';
				$output .= '</div>';
				$output .= '<div class="col-md-4 col-xs-12">';
				$output .= '<input type="text" class="form-control attr_qty" placeholder="Số lượng trong kho" name="qty" value="'.$value->attributes_qty.'">';
				$output .= '</div>';
				$output .= '<div class="col-md-4 col-xs-12">';
				$output .= '<input type="text" class="form-control attr_price" placeholder="Giá bán" name="price" value="'.$value->attributes_price.'">';
				$output .= '</div>';
				$output .= '<div class="action-column">';
				$output .= '<a href="javascript:;" class="update-attributes" data-id="'.$value->attr_id.'">Cập nhật</a>';
				$output .= '<a href="javascript:;" class="delete-attributes" data-id="'.$value->attr_id.'">Xoá</a>'; 
				$output .= '</div>';
				$output .= '</div>';
			}
		}
        // dd($attributes_value);
		echo $output;
	}
	public function update(Request $request) {
		$attr_id = $request['attr_id'];
		$attr_name = $request['attr_name'];
		$attr_qty = $request['attr_qty'];
		$attr_price = $request['attr_price'];
		AttributesProductValue::where('attr_id', $attr_id)->update([
			'attributes_value' => $attr_name,
			'attributes_qty' => $attr_qty,
			'attributes_price' => $attr_price,
		]);
		return response()->json([
			'code' => '200',
			'message' => 'product_name'
		], 200);
	}
	public function delete(Request $request) {
		$attr_id = $request['attr_id'];
		AttributesProductValue::where('attr_id', $attr_id)->delete();
		return response()->json([
			'code' => '200',
			'message' => 'product_name'
		], 200);
	}

	public function attributes_color() {
		$product_id = Session::get('product_id'); 
		$attributes_color = AttributesColorProduct::where('product_id', $product_id)->get();
		$output = ''; 
		foreach($attributes_color as $color) {
			$color_type =  $color->attributes_color_type;  
		} 
		if($attributes_color) { 
			$output .= '<div class="form-group mb-2 col-md-4 col-xs-12">';
			$output .= '<label>Màu sắc sản phẩm</label>';
			$output .= '<select disabled class="form-control color_type" name="attributes_color_type">';
			if($color_type == 1) { 
				$output .= '<option value="1" selected >Màu sắc tùy chỉnh</option>';
				$output .= '<option value="2">Hình Ảnh Minh Hoạ</option>';
			}else {
				$output .= '<option value="1"  >Màu sắc tùy chỉnh</option>';
				$output .= '<option value="2" selected>Hình Ảnh Minh Hoạ</option>';
			}
			$output .= '</select>';
			$output .= '</div>';
			$output .= '<div class="color-div mb-3">';
			$output .= '<div class="attributes_div--child">';
			$output .= '<p>Chi tiết thuộc tính </p> ';
			foreach($attributes_color as $color) {
				$output .= '<div class="attributes_item--child pr-150 row mb-2">';
				if($color_type == '1') {
					$output .= '<div class="col-md-3 col-xs-12">';
					$output .= '<div class="input-group">';
					$output .= '<input type="color" class="form-control colorpicker" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="'.$color->attributes_color_image.'">';
					$output .= '<input type="text"  pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$"  name="" value="'.$color->attributes_color_image.'" class="form-control hexcolor color_image" placeholder="#000000" style="text-transform: uppercase;">';
					$output .= '</div>';
					$output .= '</div>';
					$output .= '<div class="col-md-3 col-xs-12">';
					$output .= '<input type="text" class="form-control color_value" placeholder="Nhập màu" name="" value="'.$color->attributes_color_value.'">';
					$output .= '</div>';
					$output .= '<div class="col-md-3 col-xs-12">';
					$output .= '<input type="text" class="form-control color_qty" placeholder="Số lượng" name="" value="'.$color->attributes_color_qty.'">';
					$output .= '</div>';
					$output .= '<div class="col-md-3 col-xs-12">';
					$output .= '<input type="text" class="form-control color_price" placeholder="Giá bán" name="" value="'.$color->attributes_color_price.'">';
					$output .= '</div>';
					$output .= '<div class="action-column">';
					$output .= '<a href="javascript:;" class="update-color" data-id="'.$color->attr_id.'">Cập nhật</a>';
					$output .= '<a href="javascript:;" class="delete-color" data-id="'.$color->attr_id.'">Xoá</a>'; 
					$output .= '</div>';
				}
				else if($color_type == '2') {
					$output .= '<div class="col-md-4 col-xs-12">'; 
					$output .= '<div class="img-color-group">'; 
					$output .= '<input type="file" readonly disabled class="form-control form-file">';
					$output .= '<input type="hidden" value="'.$color->attributes_color_image.'" class="form-control file-value color_image">';
					$output .= '<img  height="100" src="'.asset('public/'.$color->attributes_color_image_path).'" class="img-color">';
					$output .= '</div></div>';
					$output .= '<div class="col-md-2 col-xs-12">';
					$output .= '<input type="text" class="form-control color_value" placeholder="Nhập màu" name="" value="'.$color->attributes_color_value.'">';
					$output .= '</div>';
					$output .= '<div class="col-md-3 col-xs-12">';
					$output .= '<input type="text" class="form-control color_qty" placeholder="Số lượng" name="attributes_color_qty[]" value="'.$color->attributes_color_qty.'">';
					$output .= '</div>';
					$output .= '<div class="col-md-3 col-xs-12">';
					$output .= '<input type="text" class="form-control color_price" placeholder="Giá bán" name="attributes_color_price[]" value="'.$color->attributes_color_price.'">';
					$output .= '</div>';
					$output .= '<div class="action-column">';
					$output .= '<a href="javascript:;" class="update-color" data-id="'.$color->attr_id.'">Cập nhật</a>';
					$output .= '<a href="javascript:;" class="delete-color" data-id="'.$color->attr_id.'">Xoá</a>'; 
					$output .= '</div>';
				}
				$output .= '</div>';
			}
			$output .= '</div>';
			if($color_type == '1') {
				$output .= '<button type="button" class="btn btn-info btn-sm add_color_item">Thêm chi tiết thuộc tính</button>';
			}
			else if($color_type == '2') {
				$output .= '<button type="button" class="btn btn-info btn-sm add_image_item">Thêm chi tiết thuộc tính</button>';
			}
			$output .= '</div>';
		} 
		echo $output;
	}

	public function delete_color(Request $request) {
		$attr_id = $request['attr_id'];
		AttributesColorProduct::where('attr_id', $attr_id)->delete();
		return response()->json([
			'code' => '200'
		], 200);
	}

	public function update_color(Request $request) {
		$attr_id = $request['attr_id'];
		$color_value = $request['color_value'];
		$color_image = $request['color_image'];
		$color_qty = $request['color_qty'];
		$color_price = $request['color_price'];
		AttributesColorProduct::where('attr_id', $attr_id)->update([
			'attributes_color_value' => $color_value,
			'attributes_color_image' => $color_image,
			'attributes_color_qty' => $color_qty,
			'attributes_color_price' => $color_price,
		]);
		return response()->json([
			'code' => '200'
		], 200);
	}

}
