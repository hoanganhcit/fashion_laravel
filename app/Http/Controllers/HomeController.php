<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Slider;
use App\ProductModel;
use App\CategoryProductModel;
use App\Gallery;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect; 
session_start();


class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function Index(Request $request)
    {

        $meta_title = 'HT Store - Sàn thời trang xuất khẩu Việt Nam';
        $meta_desc = 'HT Store - Hệ thống cửa hàng thời trang nam cao cấp hàng đầu Việt Nam. Thiết kế tinh tế, mang đến sự lịch lãm và mạnh mẽ';
        $meta_keyworks = 'HT Store, thoi trang cao cap, thoi trang nam';
        $meta_canonical = $request->url();


        $slider_list = Slider::join('tbl_category_product','tbl_category_product.category_id','=','tbl_slider.category_id')->orderby('tbl_slider.slider_id','desc')->get();

        $product_new = ProductModel::where('product_type', 1)->orderby('tbl_product.product_id', 'desc')->paginate(10); 

        $category_parent = CategoryProductModel::where('category_status','1')->where('category_parent', 0)->orderby('category_parent','desc')->get();

        foreach($category_parent as $val) {
            $category_id = $val->category_id;
        }

        // $categoryIds = CategoryProductModel::where('category_parent', $parentId = CategoryProductModel::where('category_status', 1)
        // ->value('category_id'))
        // ->pluck('category_id')
        // ->push($parentId)
        // ->all();

        // $products_by_category = ProductModel::whereIn('category_id', $categoryIds)->orderBy('product_id', 'desc')->get();

        // dd($products_by_category);  

        return view('site.home', compact('slider_list','product_new','gallery_pro','meta_title','meta_desc','meta_keyworks','meta_canonical', 'category_parent'));
    }
    public function menu(Request $request)
    { 

        $category_parent = CategoryProductModel::where('category_status','1')->where('category_parent', 0)->orderby('category_parent','desc')->get();

        foreach($category_parent as $val) {
            $category_id = $val->category_id;
        } 

        return view('site.layout.header', compact('category_parent'));
    }

    public function get_product_by_category(Request $request) {
        $category_id = $request->category_id;
        $session_cateId = Session::put('category_id', $category_id);
        return response()->json([
            'code' => '200'
        ], 200); 
    }
    public function render_product_by_category(Request $request) {
        $category_id = Session::get('category_id');
        $output = '';
        $products = ProductModel::where('category_id', $category_id)->paginate(8);
        foreach($products as $product) {
            $output .= '<div class="item">'; 
            $output .= '<div class="product type-product status-publish has-post-thumbnail">';
            $output .= '<div class="item-product item-product-grid item-product-style2">';
            $output .= '<div class="product-thumb">';
            $output .= '<a href="'.url('/san-pham/'.$product->product_slug).'" class="product-thumb-link product-thumb-link zoomout-thumb" >';
            $output .= '<img  src="'.asset('/public/'.$product->product_image_path).'" alt="'.$product->product_image.'" />';
            $output .= '<img  src="'.asset('/public/'.$product->product_image_path).'" alt="'.$product->product_image.'" />';
            $output .= '</a>';
            $output .= '<div class="product-label">';
            if($product->product_price_sale > 0) {
                $output .= '<span class="sale">Sale</span>';
            }
            $output .= '</div>';

            $output .= '<div class="product-extra-link">';
            $output .= '<button class="btn btn-primary product-quick-view quickview-link" data-bs-target="#quickview" data-bs-toggle="modal" data-id="'.$product->product_id.'"><i class="fal fa-shopping-bag"></i> <span>Mua nhanh</span></button>';
            $output .= '<a href="'.url('/san-pham/'.$product->product_slug).'" class="btn btn-primary" data-id="'.$product->product_id.'"><span>Xem chi tiết</span> <i class="fal fa-long-arrow-right"></i></a>';
            $output .= '</div> ';
            $output .= '</div>';
            $output .= '<div class="product-info">';
            $output .= '<h3 class="title14 product-title">';
            $output .= '<a title="Laborum Chair" href="'.url('/san-pham/'.$product->product_slug).'">'.$product->product_name.'</a>';
            $output .= '</h3>';
            $output .= ' <div class="product-price price variable">';
            if($product->product_price_sale > 0) {
                $output .= '<span class="price-sale">'.number_format($product->product_price).' đ</span>';
            $output .= '<span class="Price-amount">'.number_format($product->product_price_sale).' đ</span>';
            $output .= '<div class="product-label"><span class="sale-per">Giảm '.number_format((float)100 - ($product->product_price_sale / $product->product_price * 100), 0, '.', '').'%</span></div>';
            } else {
                $output .= '<span class="Price-amount">'.number_format($product->product_price).' đ</span>';
            }
            $output .= '</div>';  
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
        }
        echo $output;
    }
}
