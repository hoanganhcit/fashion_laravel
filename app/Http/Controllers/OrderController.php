<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Session;
use Auth;
use Validator;
use App\Order;
use App\OrderDetail;
use App\ProductModel;
use App\AttributesProductValue;
use App\AttributesColorProduct;
use App\Shipping;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class OrderController extends Controller
{
	public function CheckLogin() {
        if(Session::get('admin_id')){
            $admin_id = Session::get('admin_id');
        }else{
            $admin_id = Auth::id();
        }
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        } 
    }
    
    public function All_order()
    {
    	$this->CheckLogin();

        $orders = Order::where('order_status',2)->get();
        $order_new = Order::where('order_status',0)->get();
        $order_delivery = Order::where('order_status',1)->get();
        $order_destroy = Order::where('order_status',3)->get(); 

    	$order_all = Order::join('tbl_customers','tbl_customers.customer_id','=','tbl_order.customer_id')
   		->select('tbl_order.*','tbl_customers.customer_name')->orderBy('tbl_order.order_id', 'desc')->get();
        return view('admin.template.orders.orders', compact('order_all','orders','order_new','order_delivery','order_destroy'));
    }

    public function view_order_detail($order_id)
    {
        
    	$this->CheckLogin();

        $order_items = OrderDetail::join('tbl_order','tbl_order.order_id','=','tbl_order_details.order_id')
        ->join('tbl_product','tbl_product.product_id','=','tbl_order_details.product_id')->where('tbl_order_details.order_id', $order_id)->select('tbl_order.order_id','tbl_order_details.*', 'tbl_product.product_image_path')->get();

        $data_order = Order::join('tbl_customers','tbl_order.customer_id','=','tbl_customers.customer_id')
        ->join('tbl_shipping','tbl_order.shipping_id','=','tbl_shipping.shipping_id')->where('tbl_order.order_id', $order_id)
        ->select('tbl_order.*','tbl_customers.*','tbl_shipping.*')->first();

        $fee = 30000;

        return view('admin.template.orders.view_order')->with('order_items', $order_items)->with('data_order', $data_order)->with('fee', $fee);
    }
    public function edit_order_detail($order_id)
    {
        
        $this->CheckLogin();

        $order_items = OrderDetail::join('tbl_order','tbl_order.order_id','=','tbl_order_details.order_id')
        ->join('tbl_product','tbl_product.product_id','=','tbl_order_details.product_id')->where('tbl_order_details.order_id', $order_id)->select('tbl_order.order_id','tbl_order_details.*', 'tbl_product.product_image_path')->get();

        Session::put('order_items', $order_items);

        $data_order = Order::join('tbl_customers','tbl_order.customer_id','=','tbl_customers.customer_id')
        ->join('tbl_shipping','tbl_order.shipping_id','=','tbl_shipping.shipping_id')->where('tbl_order.order_id', $order_id)
        ->select('tbl_order.*','tbl_customers.*','tbl_shipping.*')->first();

        $fee = 30000;

        return view('admin.template.orders.edit_order')->with('order_items', $order_items)->with('data_order', $data_order)->with('fee', $fee);
    }

    public function update_order(Request $request,$order_id) {

        $data = array();

        $order_items = Session::get('order_items');  
        // dd($order_items);
        if($request->order_status == 2) {
            foreach($order_items as $items) {
                $product_id = $items->product_id;
                $attributes_size = $items->product_size;
                $attributes_color = $items->product_color;

                $product = ProductModel::where('product_id',$product_id)->first();
                $product->product_qty = $product->product_qty - $items->product_qty;
                $product->save();

                $attributes_value = AttributesProductValue::where('product_id',$product_id)->where('attributes_value',$attributes_size)->first();
                $attributes_value->attributes_qty = $attributes_value->attributes_qty - $items->product_qty;
                $attributes_value->save();

                $attributes_color = AttributesColorProduct::where('product_id',$product_id)->where('attributes_color_value',$attributes_color)->first();
                $attributes_color->attributes_color_qty = $attributes_color->attributes_color_qty - $items->product_qty;
                $attributes_color->save();
            }
        }

        $data['order_status'] = $request->order_status; 
        $data['order_date'] = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d');
        Order::where('order_id', $order_id)->update($data);
        Session::put('message','Cập nhật trạng thái đơn hàng thành công');
        return Redirect::to('orders');
    }

    public function destroy(Request $request) { 

        $order_id = $request['order_id'];

        Order::where('order_id', $order_id)->update(
            [
                'order_status' => '3'
            ]
        );
        return response()->json([
            'code' => '200'
        ], 200);
    }

    public function re_order(Request $request) { 

        $order_id = $request['order_id'];

        Order::where('order_id', $order_id)->update(
            [
                'order_status' => '0'
            ]
        );
        return response()->json([
            'code' => '200'
        ], 200);
    }

    public function delete_order($order_id) {

        DB::table('tbl_order')->where('order_id', $order_id)->delete();
        Session::put('message','Xoá đơn hàng thành công');
        return Redirect::to('orders');
    }
}
