<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Slider;
use Auth;
use App\CategoryProductModel;
use App\Components\Recusive;
use Illuminate\Support\Facades\Redirect;
session_start();

class SliderController extends Controller
{
    private $category;
    public function __construct(CategoryProductModel $category) {
        $this->category = $category;
    }
    public function CheckLogin() {
        if(Session::get('admin_id')){
            $admin_id = Session::get('admin_id');
        }else{
            $admin_id = Auth::id();
        }
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        } 
    }

    public function getCategory($CategoryParent) {
        $data = $this->category->all();
        $recusive = new Recusive($data);
        $htmlOption = $recusive->CategoryRecusive($CategoryParent);
        return $htmlOption;
    }
    public function all_slider() {
        $this->CheckLogin();
    	$slider_list = Slider::join('tbl_category_product','tbl_category_product.category_id','=','tbl_slider.category_id')->orderby('tbl_slider.slider_id','desc')->get();
    	return view('admin.template.slider.all_slider', compact('slider_list'));
    }
    public function add_slider () {
        $this->CheckLogin();
        $htmlOption = $this->getCategory($CategoryParent = '');
        return view('admin.template.slider.add_slider', compact('htmlOption'));
    }
    public function save_slider(Request $request){
        $data = array();

        $data['slider_title'] = $request->slider_title;
        $data['slider_subtitle'] = $request->slider_subtitle;
        $data['category_id'] = $request->category_slider;
        $data['slider_image'] = $request->slider_image;

        $get_image = $request->file('slider_image');

        if($get_image) {

        	$get_image_name = $get_image->getClientOriginalName();
        	$name_image = current(explode('.', $get_image_name));
        	$new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();

        	$get_image->move('public/uploads/sliders', $new_image);

        	$data['slider_image'] = $new_image;

        	Slider::insert($data);
        	Session::put('message','Thêm slider thành công');
        	return Redirect::to('all-slider');
        }

        Slider::insert($data);
        Session::put('message','Thêm slider thành công');
        return Redirect::to('all-slider');
    }
    public function edit_slider($slider_id) {
        $this->CheckLogin();
        $category = Slider::where('slider_id', $slider_id)->first();
        $htmlOption = $this->getCategory($CategoryParent = $category->category_id);
    	$edit_slider = Slider::where('slider_id', $slider_id)->get();
    	return view('admin.template.slider.edit_slider', compact('edit_slider','htmlOption'));
    }
    public function update_slider(Request $request,$slider_id) {
    	$data = array();

        $data['slider_title'] = $request->slider_title;
        $data['slider_subtitle'] = $request->slider_subtitle;
        $data['category_id'] = $request->category_slider;

        $get_image = $request->file('slider_image');

        if($get_image) {

        	$get_image_name = $get_image->getClientOriginalName();
        	$name_image = current(explode('.', $get_image_name));
        	$new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();

        	$get_image->move('public/uploads/sliders', $new_image);

        	$data['slider_image'] = $new_image;

        	DB::table('tbl_slider')->where('slider_id', $slider_id)->update($data);
        	Session::put('message','Cập nhật slider thành công');
        	return Redirect::to('all-slider');
        }

        DB::table('tbl_slider')->where('slider_id', $slider_id)->update($data);
        Session::put('message','Cập nhật slider thành công');
        return Redirect::to('all-slider');
    }
    public function delete_slider($slider_id) {
        DB::table('tbl_slider')->where('slider_id', $slider_id)->delete();
        Session::put('message','Xoá slider thành công');
        return Redirect::to('all-slider');
    }
}
