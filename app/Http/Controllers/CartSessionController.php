<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use App\ProductModel;
use App\Gallery;
use App\Brands;
use App\CategoryProductModel;
use App\AttributesColorProduct;
use Illuminate\Support\Str;
use App\Exports\ExcelExports;
use App\Imports\ExcelImports;
use Session;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect; 
session_start();

class CartSessionController extends Controller
{
        // Save card bằng Session
	public function save_cart (Request $request) {
		$data = $request->all(); 
		$product_id = $data['product_id'];
		$size = $data['size'];
		$color = $data['color'];
		$quantity = $data['quantity'];
		$products = ProductModel::find($product_id);
		$products_by_color = AttributesColorProduct::where('product_id', $product_id)->where('attributes_color_value', $color)->first(); 
		$session_id = substr(md5(microtime()),rand(0,26),5);

		$cart = Session::get('cart');

		if( isset($cart[$product_id])) {
			$cart[$product_id]['qty'] = $cart[$product_id]['qty'] + $quantity;
		} else 
		{  
			$cart[$product_id] = 
			[
				'session_id' => $session_id,
				'product_id' => $product_id,
				'product_name' => $products->product_name,
				'product_image_path' => $products->product_image_path,
				'qty' => $quantity,
				'product_price' => $products_by_color->attributes_color_price,
				'product_slug' => $products->product_slug,
				'product_price_sale' => $products->product_price_sale,
				'size' => $size,
				'color' => $color
			]; 
		}

		Session::put('cart', $cart);
		Session::put('product_name', $products->product_name);
		return response()->json([
			'code' => '200',
			'message' => 'product_name'
		], 200);
	}  

    // delete cart with ajax

	public function remove_cart_item($session_id) {
		$cart = Session::get('cart');
		$output = '';
		$total = 0;
		if(Session::has('cart')) {
			foreach($cart as $key => $cartItem) {
				if($cartItem['session_id'] == $session_id) {
					unset($cart[$key]);
				}
			}
			Session::put('cart', $cart);
			return redirect()->back();
		} else {
			return redirect()->back();
		}
		echo $output;
	}
	public function remove_cart_item_ajax($session_id) {
		$cart = Session::get('cart');
		$output = '';
		$total = 0;
		if(Session::has('cart')) {
			foreach($cart as $key => $cartItem) {
				if($cartItem['session_id'] == $session_id) {
					unset($cart[$key]);
				}
			}
			Session::put('cart', $cart);
			$output.='<div class="mini-cart-has-product"><div class="product-mini-cart list-mini-cart-item">';
			foreach(Session::get('cart') as $key => $cartItem){

				if($cartItem['product_price_sale'] > 0) {
					$price = $cartItem['product_price_sale'];
					$subtotal = $cartItem['qty'] * $price;
				} 
				else {
					$price = $cartItem['product_price'];
					$subtotal = $cartItem['qty'] * $price;
				}

				$total += $subtotal;


				$output.='<div class="item-info-cart product-mini-cart table-custom mini_cart_item">';
				$output.='<div class="product-thumb">';
				$output.='<a href="'.url('/san-pham/'. $cartItem['product_slug']).'">';
				$output.='<img width="50" height="100" src="'.asset('public/'. $cartItem['product_image_path']).'" alt="'.$cartItem['product_name'].'"></a></div>';
				$output.='<div class="product-info">';
				$output.='<h3 class="title14 product-title">';
				$output.='<a href="'.url('/san-pham/'.$cartItem['product_slug']).'">'.$cartItem['product_name'].'</a></h3>';
				$output.='<div class="d-flex variations-attr"><span>'.$cartItem['color'].'</span>'.'&nbsp;&nbsp;-&nbsp;&nbsp;'.'<span>'.$cartItem['size'].'</span></div>';
				$output.='<div class="mini-cart-qty">';
				$output.='<span class="qty-num">'.$cartItem['qty'].'</span> x <span class="flex-wrap">';
				$output.='<span class="price-product">'.number_format($price).' ₫</span></span></div>';
				$output.='<div class="product-delete text-right">';
				$output.='<a href="javascript:;" data-url="'.url('/remove-cart-item/'. $cartItem['session_id']).'" class="remove-product remove-this-item"><i class="fal fa-trash"></i></a></div>';
				$output.='</div></div>';
			}  
			$output.='</div>';
			$output.='<div class="mini-cart-total text-uppercase title18 clearfix">';
			$output.='<span class="pull-left">Tông tiền tạm tính</span>';
			$output.='<strong class="pull-right color mini-cart-total-price get-cart-price">'.number_format($total).' đ</strong></div>';
			$output.='<div class="mini-cart-button">';
			if(Session::get('customer_id')) {
				$output.='<a href="'.url('/checkout').'" class="button checkout wc-forward">Thanh Toán</a>';
			} else {
				$output.='<a href="'.url('/customer/login').'" class="button wc-forward">Thanh Toán</a>';
			}
			$output.='<a href="'.url('/your-cart').'" class="btn-link">Xem giỏ hàng <i class="fal fa-arrow-right"></i></a>';
			$output.='</div></div>';
		} else {
			return redirect()->back();
		}
		echo $output;
	}

    // update cart

	public function update_cart(Request $request) {
		$data = $request->all();
		$cart = Session::get('cart');
		if($cart==true) {
			foreach($data['quantity_update'] as $key => $qty) {
				foreach($cart as $session => $val) {
					if($val['session_id'] == $key) {
						$cart[$session]['qty'] = $qty;
					}
				}
			}
			Session::put('cart', $cart);
			return redirect()->back();
		} else {
			return redirect()->back();
		}
	}

    // your cart
	public function show_cart(Request $request) {

		$meta_title = 'Giỏ hàng';
		$meta_desc = 'HT Store - Hệ thống cửa hàng thời trang nam cao cấp hàng đầu Việt Nam. Thiết kế tinh tế, mang đến sự lịch lãm và mạnh mẽ';
		$meta_keyworks = 'HT Store, thoi trang cao cap, thoi trang nam';
		$meta_canonical = $request->url();

		$cart = Session::get('cart');

        $category_parent = CategoryProductModel::where('category_status','1')->where('category_parent', 0)->orderby('category_parent','desc')->get();
		return view('site.cart.cart', compact('cart','meta_title','meta_desc','meta_keyworks','meta_canonical', 'category_parent'));
	}

	public function count_cart() {
		$cart = Session::get('cart');
		$output = '';
		if(count($cart) == 0) {
			$output = 0;
		} else {
			$output.= $cart;
		}
		echo $output;
	}
	public function mini_cart_ajax() { 
		$cart = count(Session::get('cart'));
		$output = '';                          
		$total = 0;

		if(Session::get('cart') == NULL) {
			$output.='<div class="mini-cart-empty">';
			$output.='<i class="fal fa-shopping-cart title120 empty-icon"></i>';
			$output.='<p class="mt-4">Hiện chưa có sản phẩm</p>';
			$output.='</div>';                                            
		} else {
			$output.='<div class="mini-cart-has-product"><div class="product-mini-cart list-mini-cart-item">';
			foreach(Session::get('cart') as $key => $cartItem){

				if($cartItem['product_price_sale'] > 0) {
					$price = $cartItem['product_price_sale'];
					$subtotal = $cartItem['qty'] * $price;
				} 
				else {
					$price = $cartItem['product_price'];
					$subtotal = $cartItem['qty'] * $price;
				}

				$total += $subtotal;


				$output.='<div class="item-info-cart product-mini-cart table-custom mini_cart_item">';
				$output.='<div class="product-thumb">';
				$output.='<a href="'.url('/san-pham/'. $cartItem['product_slug']).'">';
				$output.='<img width="50" height="100" src="'.asset('public/'. $cartItem['product_image_path']).'" alt="'.$cartItem['product_name'].'"></a></div>';
				$output.='<div class="product-info">';
				$output.='<h3 class="title14 product-title">';
				$output.='<a href="'.url('/san-pham/'.$cartItem['product_slug']).'">'.$cartItem['product_name'].'</a></h3>';
				$output.='<div class="d-flex variations-attr"><span>'.$cartItem['color'].'</span>'.'&nbsp;&nbsp;-&nbsp;&nbsp;'.'<span>'.$cartItem['size'].'</span></div>';
				$output.='<div class="mini-cart-qty">';
				$output.='<span class="qty-num">'.$cartItem['qty'].'</span> x <span class="flex-wrap">';
				$output.='<span class="price-product">'.number_format($price).' ₫</span></span></div>';
				$output.='<div class="product-delete text-right">';
				$output.='<a href="javascript:;" data-url="'.url('/remove-cart-item/'. $cartItem['session_id']).'" class="remove-product remove-this-item"><i class="fal fa-trash"></i></a></div>';
				$output.='</div></div>';
			}  
			$output.='</div>';
			$output.='<div class="mini-cart-total text-uppercase title18 clearfix">';
			$output.='<span class="pull-left">Tông tiền tạm tính</span>';
			$output.='<strong class="pull-right color mini-cart-total-price get-cart-price">'.number_format($total).' đ</strong></div>';
			$output.='<div class="mini-cart-button">';
			if(Session::get('customer_id')) {
				$output.='<a href="'.url('/checkout').'" class="button checkout wc-forward">Thanh Toán</a>';
			} else {
				$output.='<a href="'.url('/customer/login').'" class="button wc-forward">Thanh Toán</a>';
			}
			$output.='<a href="'.url('/your-cart').'" class="btn-link">Xem giỏ hàng <i class="fal fa-arrow-right"></i></a>';
			$output.='</div></div>';
		}

		echo $output;
	}
 
}
