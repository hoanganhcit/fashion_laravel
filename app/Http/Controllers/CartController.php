<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use App\ProductModel;
use App\Gallery;
use App\Brands;
use App\CategoryProductModel;
use App\AttributesColorProduct;
use Illuminate\Support\Str;
use App\Exports\ExcelExports;
use App\Imports\ExcelImports;
use Session;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect; 
session_start();

class CartController extends Controller
{

    // Cart with vendor Gloudemans Shopping Cart
    
    // Cart with Ajax
    // add to cart with form
    public function add_to_cart(Request $request) { 
        $request = $request->all();
        $data = array(); 
        $product_id = $request['product_id'];
        $size = $request['size'];
        $color = $request['color'];
        $quantity = $request['quantity'];
        $products = ProductModel::find($product_id);
        $products_by_color = AttributesColorProduct::where('product_id', $product_id)->where('attributes_color_value', $color)->first(); 

        $data['id'] = $product_id;
        $data['qty'] = $quantity;
        $data['name'] = $products->product_name;
        $data['weight'] = 1;
        $data['taxRate'] = 0;
        $data['price'] = $products->product_price;
        $data['options']['product_image_path'] = $products->product_image_path;
        $data['options']['product_slug'] = $products->product_slug; 
        $data['options']['product_price_sale'] = $products->product_price_sale;
        $data['options']['attributes_color_price'] = $products_by_color->attributes_color_price;
        $data['options']['size'] = $size;
        $data['options']['color'] = $color;

        Cart::add($data);
        // Cart::destroy();

        $content = Cart::content();
        Session::put('product_name', $products->product_name);
        return response()->json([
            'code' => '200',
            'message' => 'product_name'
        ], 200);
    }
    public function mini_cart() { 
        $cart = Cart::content();
        $output = '';                             
        $total = 0;
        if(Cart::count() == 0) {
           $output.='<div class="mini-cart-empty">';
           $output.='<i class="fal fa-shopping-cart title120 empty-icon"></i>';
           $output.='<p class="mt-4">Hiện chưa có sản phẩm</p>';
           $output.='</div>';                                            
        } else {
            $output.='<div class="mini-cart-has-product"><div class="product-mini-cart list-mini-cart-item">';
            foreach($cart as $key => $cartItem){

                if($cartItem->options['product_price_sale'] > 0) {
                    $price = $cartItem->options['product_price_sale'];
                    $subtotal = $cartItem->qty * $price;
                } 
                else {
                    $price = $cartItem->price;
                    $subtotal = $cartItem->qty * $price;
                }

                $total += $subtotal;


                $output.='<div class="item-info-cart product-mini-cart table-custom mini_cart_item">';
                $output.='<div class="product-thumb">';
                $output.='<a href="'.url('/san-pham/'. $cartItem->options['product_slug']).'">';
                $output.='<img width="50" height="100" src="'.asset('public/'. $cartItem->options['product_image_path']).'" alt="'.$cartItem->name.'"></a></div>';
                $output.='<div class="product-info">';
                $output.='<h3 class="title14 product-title">';
                $output.='<a href="'.url('/san-pham/'.$cartItem->options['product_slug']).'">'.$cartItem->name.'</a></h3>';
                $output.='<div class="d-flex variations-attr"><span>'.$cartItem->options['color'].'</span>'.'&nbsp;&nbsp;-&nbsp;&nbsp;'.'<span>'.$cartItem->options['size'].'</span></div>';
                $output.='<div class="mini-cart-qty">';
                $output.='<span class="qty-num">'.$cartItem->qty.'</span> x <span class="flex-wrap">';
                $output.='<span class="price-product">'.number_format($price).' ₫</span></span></div>';
                $output.='<div class="product-delete text-right">';
                $output.='<a href="javascript:;" data-url="'.url('/delete-cart-item/'. $cartItem->rowId).'" class="remove-product remove-this-item"><i class="fal fa-trash"></i></a></div>';
                $output.='</div></div>';
            }  
            $output.='</div>';
            $output.='<div class="mini-cart-total text-uppercase title18 clearfix">';
            $output.='<span class="pull-left">Tông tiền tạm tính</span>';
            $output.='<strong class="pull-right color mini-cart-total-price get-cart-price">'.number_format($total).' đ</strong></div>';
            $output.='<div class="mini-cart-button">';
            if(Session::get('customer_id')) {
                $output.='<a href="'.url('/checkout').'" class="button checkout wc-forward">Thanh Toán</a>';
            } else {
                $output.='<a href="'.url('/customer/login').'" class="button wc-forward">Thanh Toán</a>';
            }
            $output.='<a href="'.url('/view-cart').'" class="btn-link">Xem giỏ hàng <i class="fal fa-arrow-right"></i></a>';
            $output.='</div></div>';
        }

        echo $output;
    }
    public function count_cart() {
        $cart = Cart::count();
        $output = '';
        if($cart == 0) {
            $output = 0;
        } else {
            $output.= $cart;
        }
        echo $output;
    }
    public function delete_cart_item_ajax($rowId) {
        $cart = Cart::content();
        $output = '';
        $total = 0;
        if($cart) {
            foreach($cart as $key => $cartItem) {
                if($cartItem->rowId == $rowId) {
                    Cart::remove($rowId);
                }
            }
            $cart = Cart::content();
            $output.='<div class="mini-cart-has-product"><div class="product-mini-cart list-mini-cart-item">';
            foreach($cart as $key => $cartItem){

                if($cartItem->options['product_price_sale'] > 0) {
                    $price = $cartItem->options['product_price_sale'];
                    $subtotal = $cartItem->qty * $price;
                } 
                else {
                    $price = $cartItem->price;
                    $subtotal = $cartItem->qty * $price;
                }

                $total += $subtotal;


                $output.='<div class="item-info-cart product-mini-cart table-custom mini_cart_item">';
                $output.='<div class="product-thumb">';
                $output.='<a href="'.url('/san-pham/'. $cartItem->options['product_slug']).'">';
                $output.='<img width="50" height="100" src="'.asset('public/'. $cartItem->options['product_image_path']).'" alt="'.$cartItem->name.'"></a></div>';
                $output.='<div class="product-info">';
                $output.='<h3 class="title14 product-title">';
                $output.='<a href="'.url('/san-pham/'.$cartItem->options['product_slug']).'">'.$cartItem->name.'</a></h3>';
                $output.='<div class="d-flex variations-attr"><span>'.$cartItem->options['color'].'</span>'.'&nbsp;&nbsp;-&nbsp;&nbsp;'.'<span>'.$cartItem->options['size'].'</span></div>';
                $output.='<div class="mini-cart-qty">';
                $output.='<span class="qty-num">'.$cartItem->qty.'</span> x <span class="flex-wrap">';
                $output.='<span class="price-product">'.number_format($price).' ₫</span></span></div>';
                $output.='<div class="product-delete text-right">';
                $output.='<a href="javascript:;" data-url="'.url('/delete-cart-item/'. $cartItem->rowId).'" class="remove-product remove-this-item"><i class="fal fa-trash"></i></a></div>';
                $output.='</div></div>';
            }  
            $output.='</div>';
            $output.='<div class="mini-cart-total text-uppercase title18 clearfix">';
            $output.='<span class="pull-left">Tông tiền tạm tính</span>';
            $output.='<strong class="pull-right color mini-cart-total-price get-cart-price">'.number_format($total).' đ</strong></div>';
            $output.='<div class="mini-cart-button">';
            if(Session::get('customer_id')) {
                $output.='<a href="'.url('/checkout').'" class="button checkout wc-forward">Thanh Toán</a>';
            } else {
                $output.='<a href="'.url('/customer/login').'" class="button wc-forward">Thanh Toán</a>';
            }
            $output.='<a href="'.url('/your-cart').'" class="btn-link">Xem giỏ hàng <i class="fal fa-arrow-right"></i></a>';
            $output.='</div></div>';
        } else {
            return redirect()->back();
        }
        echo $output;
    } 

    public function update_your_cart(Request $request) {
        $data = $request->all();
        $cart = Cart::content();
        if($cart==true) {
            foreach($data['quantity_update'] as $key => $qty) {
                foreach($cart as $rowId => $val) {  
                    if($val->rowId == $key) { 
                        $val->qty = $qty;
                        $val->rowId = $rowId;
                    }
                }
            }
            Cart::update($rowId, $qty);
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }
    
    // delete cart item
    public function delete_item_your_cart($rowId) {
        Cart::remove($rowId);
        return redirect()->back();
    }

    // show your cart   
    public function view_cart(Request $request) {
        $cart = Cart::content();
        $meta_title = 'Giỏ hàng';
        $meta_desc = 'HT Store - Hệ thống cửa hàng thời trang nam cao cấp hàng đầu Việt Nam. Thiết kế tinh tế, mang đến sự lịch lãm và mạnh mẽ';
        $meta_keyworks = 'HT Store, thoi trang cao cap, thoi trang nam';
        $meta_canonical = $request->url();
        $category_parent = CategoryProductModel::where('category_status','1')->where('category_parent', 0)->orderby('category_parent','desc')->get();
        return view('site.cart.view_cart', compact('cart','meta_title','meta_desc','meta_keyworks','meta_canonical','category_parent'));
    }
}
