<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\ProductModel;
use App\Gallery;
use App\Brands;
use App\CategoryProductModel;
use App\AttributesProduct;
use App\AttributesProductValue;
use App\AttributesColorProduct;
use Illuminate\Support\Str;
use App\Exports\ExcelExports;
use App\Imports\ExcelImports;
use Session;
use Auth;
use Storage;
use App\Traits\UploadImageStrait;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect; 
use App\Components\Recusive;
session_start();

class ProductController extends Controller
{
    use UploadImageStrait;
    private $category;
    public function __construct(CategoryProductModel $category) {
        $this->category = $category;
    }

    public function CheckLogin() {
        if(Session::get('admin_id')){
            $admin_id = Session::get('admin_id');
        }else{
            $admin_id = Auth::id();
        }
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        } 
    }

    public function all_product() {
        $this->CheckLogin();
        $product_brand = ProductModel::join('tbl_brand','tbl_brand.brand_id','=','tbl_product.brand_id')
        ->orderby('tbl_product.product_id','desc')->get();
    	$all_product = ProductModel::join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')
        ->orderby('tbl_product.product_id','desc')->get();
    	return view('admin.template.products.all_product_list', compact('all_product','product_brand'));
    }

    public function getCategory($CategoryParent) {
        $data = $this->category->all();
        $recusive = new Recusive($data);
        $htmlOption = $recusive->CategoryRecusive($CategoryParent);
        return $htmlOption;
    }

    public function add_product()
    {
        $this->CheckLogin();
    	$brand_product = Brands::orderby('brand_id', 'desc')->get();
        $htmlOption = $this->getCategory($CategoryParent = '');
        return view('admin.template.products.product_add_new', compact('htmlOption', 'brand_product'));
    }

    public function save_product(Request $request){

        $this->validate($request, [
            'product_name' => 'required',
            'product_price' => 'required',
            'product_qty' => 'required',
            'product_sku' => 'required',
            'product_desc' => 'required',
        ]);
        
        $data['product_name'] = $request->product_name;
        $data['product_slug'] = $request->product_slug;
        $data['product_price'] = $request->product_price;
        $data['product_price_sale'] = $request->product_price_sale;
        $data['product_qty'] = $request->product_qty;
        $data['product_sku'] = $request->product_sku;
        $data['product_status'] = $request->product_status;
        $data['product_desc'] = $request->product_desc;
        $data['product_type'] = $request->product_type;
        $data['product_content'] = $request->product_content;
        $data['product_keywords'] = $request->product_keywords;
        $data['category_id'] = $request->category_product;
        $data['brand_id'] = $request->brand_product;

        $dataUploadProductImage = $this->StorageImageUpload($request, $fieldName = 'product_image', $folderName = $request->product_slug);

        if(!empty($dataUploadProductImage)) {
            $data['product_image'] = $dataUploadProductImage['file_name'];
            $data['product_image_path'] = $dataUploadProductImage['file_path'];
        }
        $products = ProductModel::create($data);
        $get_image = $request->file('gallery_image');
        if( $get_image) {
            foreach($get_image as $fileItem) {
                $dataUploadProductGallery = $this->StorageImageUploadMultiple($fileItem, $folderName = $products->product_id);
                Gallery::insert([
                    'product_id' => $products->product_id,
                    'gallery_image' => $dataUploadProductGallery['file_name'],
                    'gallery_image_path' => $dataUploadProductGallery['file_path'],
                ]);
            }

        } 

        $get_image = $request->file('gallery_image'); 

        if($request->isMethod('post')) {
            $attributes_id = AttributesProduct::create([
                'product_id' => $products->product_id,
                'attributes_name' => $request->attributes_name,
            ]);
        }
        if($request->isMethod('post')) {
            foreach($request->attributes_value as $key => $val) {
                $attributesValue = new AttributesProductValue;
                $attributesValue->product_id = $products->product_id;
                $attributesValue->attributes_value = $request->attributes_value[$key];
                $attributesValue->attributes_qty = $request->attributes_qty[$key];
                $attributesValue->attributes_price = $request->attributes_price[$key];
                $attributesValue->save();
            }
        }

        $type_color_attr = $request->attributes_color_type;

        if($type_color_attr == 1) {

            foreach($request->attributes_color_value as $key => $val) { 
                $attributesColor = new AttributesColorProduct;
                $attributesColor->product_id = $products->product_id;
                $attributesColor->attributes_color_type = $type_color_attr;
                $attributesColor->attributes_color_value = $request->attributes_color_value[$key];
                $attributesColor->attributes_color_qty = $request->attributes_color_qty[$key];
                $attributesColor->attributes_color_price = $request->attributes_color_price[$key];
                $attributesColor->attributes_color_image = $request->attributes_color_image[$key];
                $attributesColor->attributes_color_image_path = '';
                $attributesColor->save();
            }
        }else { 
            $get_ImageColor = $request->file('attributes_color_image');   
            if(!empty($get_ImageColor)) {
                foreach($get_ImageColor as $key => $fileItem) {   
                    $dataImageColor = $this->StorageImageUploadVariationColor($fileItem, $folderName = $products->product_id);
                    $attributesColor = new AttributesColorProduct;
                    $attributesColor->product_id = $products->product_id;
                    $attributesColor->attributes_color_type = $type_color_attr;
                    $attributesColor->attributes_color_value = $request->attributes_color_value[$key];
                    $attributesColor->attributes_color_qty = $request->attributes_color_qty[$key];
                    $attributesColor->attributes_color_price = $request->attributes_color_price[$key];
                    $attributesColor->attributes_color_image = $dataImageColor['file_name'];
                    $attributesColor->attributes_color_image_path = $dataImageColor['file_path'];
                    $attributesColor->save();
                }
            }
        }

        Session::put('message','Thêm sản phẩm thành công');
        return Redirect::to('all-product-list');
        return redirect()->back();
    }

    public function edit_product($product_id) {
        $this->CheckLogin();

    	$brand = Brands::orderBy('brand_id','DESC')->get();

    	$edit_product = ProductModel::where('product_id', $product_id)->get();
        Session::put('product_id', $product_id);

        $attributes_product = AttributesProductValue::where('product_id', $product_id)->get();

        $attributes_color = AttributesColorProduct::where('product_id', $product_id)->get();

        $gallerys = Gallery::where('product_id', $product_id)->get();

        foreach($edit_product as $val) {
            $parentID = $val->category_id;
        }
        $htmlOption = $this->getCategory($parentID);
    	return view('admin.template.products.edit_product', compact('edit_product', 'htmlOption', 'brand', 'gallerys', 'attributes_value','attributes_color'));
    }
    public function update_product(Request $request,$product_id) {

        $this->validate($request, [
            'product_name' => 'required',
            'product_price' => 'required',
            'product_qty' => 'required',
            'product_sku' => 'required',
            'product_desc' => 'required',
        ]);

    	$data = array();

        $data['product_name'] = $request->product_name;
        $data['product_slug'] = $request->product_slug;
        $data['product_price'] = $request->product_price;
        $data['product_price_sale'] = $request->product_price_sale;
        $data['product_qty'] = $request->product_qty;
        $data['product_sku'] = $request->product_sku;
        $data['product_status'] = $request->product_status;
        $data['product_desc'] = $request->product_desc;
        $data['product_content'] = $request->product_content;
        $data['product_keywords'] = $request->product_keywords;
        $data['category_id'] = $request->category_product;
        $data['brand_id'] = $request->brand_product;

        $dataUploadProductImage = $this->StorageImageUpload($request, $fieldName = 'product_image', $folderName = $request->product_slug);

        if(!empty($dataUploadProductImage)) {
            $data['product_image'] = $dataUploadProductImage['file_name'];
            $data['product_image_path'] = $dataUploadProductImage['file_path'];
        }

        ProductModel::where('product_id', $product_id)->update($data);

        if($request->attributes_value) {
            foreach($request->attributes_value as $key => $val) {
                $attributesValue = new AttributesProductValue;
                $attributesValue->product_id = $product_id;
                $attributesValue->attributes_value = $request->attributes_value[$key];
                $attributesValue->attributes_qty = $request->attributes_qty[$key];
                $attributesValue->attributes_price = $request->attributes_price[$key];
                $attributesValue->save();
            }
        }

        $type_color_attr = $request->attributes_color_type;

        if($type_color_attr == 1) {

            foreach($data['attributes_color_value'] as $key => $val) { 
                $attributesColor = new AttributesColorProduct;
                $attributesColor->product_id = $product_id;
                $attributesColor->attributes_color_type = $type_color_attr;
                $attributesColor->attributes_color_value = $data['attributes_color_value'][$key];
                $attributesColor->attributes_color_qty = $data['attributes_color_qty'][$key];
                $attributesColor->attributes_color_price = $data['attributes_color_price'][$key];
                $attributesColor->attributes_color_image = $data['attributes_color_image'][$key];
                $attributesColor->attributes_color_image_path = '';
                $attributesColor->save();
            }
        }else { 
            $get_ImageColor = $request->file('attributes_color_image');   
            if(!empty($get_ImageColor)) {
                foreach($get_ImageColor as $key => $fileItem) {   
                    $dataImageColor = $this->StorageImageUploadVariationColor($fileItem, $folderName = $product_id);
                    $attributesColor = new AttributesColorProduct;
                    $attributesColor->product_id = $product_id;
                    $attributesColor->attributes_color_type = $type_color_attr;
                    $attributesColor->attributes_color_value = $request->attributes_color_value[$key];
                    $attributesColor->attributes_color_qty = $request->attributes_color_qty[$key];
                    $attributesColor->attributes_color_price = $request->attributes_color_price[$key];
                    $attributesColor->attributes_color_image = $dataImageColor['file_name'];
                    $attributesColor->attributes_color_image_path = $dataImageColor['file_path'];
                    $attributesColor->save();
                }
            }
        }
        Session::put('message','Cập nhật sản phẩm thành công');
        return Redirect::to('all-product-list');
    }
    public function delete_product($product_id) {
        ProductModel::where('product_id', $product_id)->delete();
        Session::put('message','Xoá sản phẩm thành công');
        return Redirect::to('all-product-list');
    }

    // FRONT END

    public function quickview(Request $request){
        $product_id = $request->product_id;
        $product = ProductModel::where('product_id', $product_id)->get();
        foreach ($product as $key => $product) {
            $output['product_image'] = '<a href="'.url('/san-pham/'. $product->product_slug).'" class="img_url"><img width="600" height="686" src="'.asset('/public'.$product->product_image_path).'" class="wp-post-image wp-post-image" alt="'.$product->product_name.'" /></a>';

            $output['product_name'] = $product->product_name;
            $output['product_name_hidden'] = '<input type="hidden" id="product_name" value="'.$product->product_name.'">';
            $output['product_status'] = '';

            if($product->product_status == 1) {
                $output['product_status'] .= '<p>Tình Trạng: <span class="text-success">Còn hàng</span></p>';
            }else {
                $output['product_status'] .= '<p>Tình Trạng:  <span class="text-danger">Hết hàng</span></p>';
            }

            $output['product_price'] = '';

            if($product->product_price_sale > 0) { 
                $output['product_price'] .= '<span class="price-sale">'.number_format($product->product_price).' đ</span>';
                $output['product_price'] .= '<span class="Price-amount">'.number_format($product->product_price_sale).' đ</span>';
            } else {
                $output['product_price'] .= '<span class="Price-amount">'.number_format($product->product_price).' đ</span>';
            }


            $output['product_button'] = '<button type="submit" name="add-to-cart" class="single_add_to_cart_button button alt" data-id="'.$product->product_id.'">Thêm vào giỏ hàng</button>';

        }
        

        $gallery = Gallery::where('product_id',$product_id)->get();

        $output['product_gallery'] = '';
        foreach($gallery as $gal) {
            $output['product_gallery'].= '<li data-number="">';
            $output['product_gallery'].= '<a href="'.asset('/public/'.$gal->gallery_image_path).'">';
            $output['product_gallery'].= '<img width="100" height="114" src="'.asset('/public'.$gal->gallery_image_path).'" class="attachment-100x114 size-100x114" alt="'.asset('/public/'.$gal->gallery_image).'" data-src="'.asset('/public'.$gal->gallery_image_path).'"  />';
            $output['product_gallery'].= '</a>';
            $output['product_gallery'].= '</li>';
        }

        $variation_color = AttributesColorProduct::join('tbl_product', 'tbl_product.product_id', '=', 'tbl_attributes_color.product_id')
        ->where('tbl_attributes_color.product_id', $product_id)
        ->select('tbl_product.product_id', 'tbl_attributes_color.*')->get();

        $output['product_color'] = '';
        foreach($variation_color as $color) {
            if($color->attributes_color_type == 1) {
                $output['product_color'].= '<a href="javascript:void(0)" data-bs-toggle="tooltip" title="'.$color->attributes_color_value.'" class="text-center d-inline-block color-item" data-value="'.$color->attributes_color_value.'">';
                $output['product_color'].= '<span class="color-variation" style="background: '.$color->attributes_color_image.'"></span>';
                $output['product_color'].= '</a>';
            } else {
                $output['product_color'].= '<a href="javascript:void(0)" data-bs-toggle="tooltip" title="'.$color->attributes_color_value.'" class="text-center d-inline-block color-item" data-value="'.$color->attributes_color_value.'">';
                $output['product_color'].= '<img src="'.asset('public/'.$color->attributes_color_image_path).'" class="color-img" width="30" alt="'.$color->attributes_color_image.'">';
                $output['product_color'].= '</a>';
            }
        }

       
        $variation_size = AttributesProductValue::join('tbl_product', 'tbl_product.product_id', '=', 'tbl_attributes_value.product_id')
        ->where('tbl_attributes_value.product_id', $product_id)
        ->select('tbl_product.product_id', 'tbl_attributes_value.*')->get();

        $output['product_size'] = '';
        foreach($variation_size as $size) {
            $output['product_size'].= '<a href="javascript:void(0)" class="text-center d-inline-block size-item" data-bs-toggle="tooltip" title="Còn lại '.$size->attributes_qty.' sản phẩm" data-value="'.$size->attributes_value.'">'.$size->attributes_value.'</a>';
        }

        echo json_encode($output);
    }

    public function detail_product(Request $request ,$product_slug) {

        $product_slug = ProductModel::where('product_slug',$product_slug)->get();

        foreach($product_slug as $key => $product){
            $product_id = $product->product_id;
            $category_id = $product->category_id;
        }

        $product = ProductModel::where('product_id',$product_id)->first();
        $product->product_view = $product->product_view + 1;
        $product->save();
        

        $galery_product_thumb = Gallery::join('tbl_product', 'tbl_product.product_id', '=', 'tbl_gallery.product_id')->where('tbl_gallery.product_id', $product_id)->get();

        $product_brand = ProductModel::join('tbl_brand','tbl_brand.brand_id','=','tbl_product.brand_id')->get();

        $data_product = ProductModel::join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')->where('product_id', $product_id)->get(); 

        $variation_color = AttributesColorProduct::join('tbl_product', 'tbl_product.product_id', '=', 'tbl_attributes_color.product_id')
        ->where('tbl_attributes_color.product_id', $product_id)
        ->select('tbl_product.product_id', 'tbl_attributes_color.*')->get();

        $variation_size = AttributesProductValue::join('tbl_product', 'tbl_product.product_id', '=', 'tbl_attributes_value.product_id')
        ->where('tbl_attributes_value.product_id', $product_id)
        ->select('tbl_product.product_id', 'tbl_attributes_value.*')->get();

        foreach ($data_product as $val) {
            $meta_title = $val->product_name;
            $meta_desc = $val->product_desc;
            $meta_keyworks = $val->product_keywords;
            $meta_canonical = $request->url();
        }

        $related_product = ProductModel::join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')
        ->where('tbl_category_product.category_id',$category_id)->whereNotIn('tbl_product.product_id',[$product_id])->orderby(DB::raw('RAND()'))->paginate(4);

        $category_parent = CategoryProductModel::where('category_status','1')->where('category_parent', 0)->orderby('category_parent','desc')->get();
        return view('site.products.product_detail',compact('product_brand','data_product','galery_product_thumb','related_product','meta_title','meta_desc','meta_keyworks','meta_canonical','variation_color','variation_size','category_parent'));
    }
}
