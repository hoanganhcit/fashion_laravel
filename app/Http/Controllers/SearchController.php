<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\ProductModel;
use Session;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class SearchController extends Controller
{
    public function searchProduct(Request $request) {
    	return ProductModel::where('product_name', 'LIKE', '%'.$request->q.'%')->get();
    }
}
