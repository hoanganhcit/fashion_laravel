<?php

namespace App\Components;
use Carbon\Carbon;
class Date 
{
	Protected $listDay;
	public static function getListDayInMonth($month) 
	{
		$arrayDay = [];
		$month = $month;
		$year = date('y');

		// Lấy tất cả các ngày trong tháng

		for($day = 1; $day <= 31 ; $day ++) {
			$time = mktime(12,0,0,$month,$day,$year);
			if (date('m', $time) == $month) {
				$arrayDay[] = date('Y-m-d', $time);
			}
		}

		return $arrayDay;
	}
	public static function getListDayInWeek($week) 
	{
		$arrayDay = [];
		$month = Carbon::now()->month;
		$weekS = $week; 
		$year = Carbon::now()->year;
		// Lấy tất cả các ngày trong tháng

		for($day = 1; $day <= 7 ; $day ++) {
			$arrayDay[] = $weekS->addDay()->format('Y-m-d');
		}

		return $arrayDay;
	}

	public static function getListDayInYear() 
	{ 
		$arrayDay = [];
		for ($i = 11; $i >= 0; $i--) {
			$month = Carbon::now()->startOfMonth()->subMonth($i);
			$year = Carbon::now()->startOfMonth()->subMonth($i)->format('Y');
			array_push($arrayDay, array(
				'month' => $month->format('M')
			));
		}
		return $arrayDay;

	}
}