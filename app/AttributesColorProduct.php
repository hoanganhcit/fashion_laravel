<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributesColorProduct extends Model
{
    public $timestamps = false; //set time to false
    protected $fillable = [
    	'product_id', 'attributes_color_type', 'attributes_color_value', 'attributes_color_qty', 'attributes_color_price','attributes_color_image', 'attributes_color_image_path'
    ];
    protected $primaryKey = 'attr_id';
    protected $table = 'tbl_attributes_color';

    public function products(){
 		return $this->hasMany(ProductModel::class, 'product_id');
 	}
}
