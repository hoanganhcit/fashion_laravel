<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 

class ProductModel extends Model
{
    public $timestamps = false; //set time to false
    protected $fillable = [
    	'product_name', 'product_slug','category_id','brand_id','product_desc','product_sku','product_content','product_price','product_price_sale','product_qty','product_image','product_status','product_keywords', 'product_image_path','product_type','product_view','product_sold'
    ];
    protected $primaryKey = 'product_id';
    protected $table = 'tbl_product';

    public function comment(){
    	return $this->hasMany(Comment::class);
    }
    public function category() {
        return $this->belongsTo(CategoryProductModel::class, 'category_id');
    }
    public function brand(){
        return $this->belongsTo(Brands::class,'brand_id');
    }

    public function gallery(){
        return $this->hasOne(Gallery::class);
    }

    public function attributes_value(){
        return $this->hasMany(AttributesProductValue::class);
    }
    public function attributes_color(){
        return $this->hasMany(AttributesColorProduct::class);
    } 
}
