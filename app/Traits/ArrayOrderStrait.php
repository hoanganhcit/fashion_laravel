<?php

namespace App\Traits;
use Storage;
use Session; 
use App\Order;
trait ArrayOrderStrait {

	public function arrayRevenueOrderGet($listDay,$dateS,$dateE)
	{   
		$dateS = $dateS;
		$dateE = $dateE;
		$revenueOrderMonth = Order::where('order_status', 2)
		->whereBetween('order_date',[$dateS, $dateE])
		->select(\DB::raw('sum(order_total) as order_total'), \DB::raw('DATE(order_date) order_date'))
		->groupBy('order_date')
		->get()->toArray(); 
		
		$arrayRevenueOrderMonth = [];

		foreach($listDay as $day) {
			$total = 0;
			foreach($revenueOrderMonth as $key => $revenue) {
				if( $revenue['order_date'] == $day) {
					$total = $revenue['order_total'];
					break;
				}
			}

			$arrayRevenueOrderMonth[] = (int)$total;

		}
		return $arrayRevenueOrderMonth;
	} 

	public function arrayCountOrderCount($listDay,$dateS,$dateE)
	{ 
		$dateS = $dateS;
		$dateE = $dateE;
		$countOrderMonth = Order::where('order_status', 2)
		->whereBetween('order_date',[$dateS, $dateE])
		->select(\DB::raw('count(order_total) as order_total'), \DB::raw('DATE(order_date) order_date'))
		->groupBy('order_date')
		->get()->toArray(); 


		$arrayCountOrderMonth = []; 

		foreach($listDay as $day) {
			$count = 0;
			foreach($countOrderMonth as $key => $revenue) {
				if( $revenue['order_date'] == $day) {
					$count = $revenue['order_total'];
					break;
				}
			}

			$arrayCountOrderMonth[] = (int)$count;

		} 
		return $arrayCountOrderMonth;
	} 
}